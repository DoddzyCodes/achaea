local people_db
local people

local currentHonours = ""

local defaultValues = {
    name = "",
	city = "None",
	class = "Unknown",
	order = "",
	race = "",
	rank = 0,
	might = 0,
	is_dragon = "",
	max_health = 0,
	kills_to = 0,
	deaths_to = 0,
	is_house_enemy = "false",
	is_city_enemy = "false",
	is_order_enemy = "false",
	is_personal_enemy = "false",
	is_personal_ally = "false",
}

local function updateDetails(info)
    if not info.name then return end; --TODO: ADD error checking
    info.name = info.name:title()
    
    local entry = db:fetch(people, db:eq(people.name, info.name))[1];
    if(entry) then
       
        for k, v in pairs(info) do
            entry[k] = v
        end
        db:update(people, entry)
    else
        --Make sure we push all the correct defaults as well as what we have set
        for k, v in pairs(defaultValues) do
            info[k] = info[k] or v
        end
        local e, msg = db:add(people, info)
        if not e then display(msg) end
    end
  
    --Marvin.Offensive.Targetting.CheckForTargetDataUpdate();
    --local res = db:add(people, info)
end

local function getDetails(name)
    name = name:title()
    local entry = db:fetch(people, db:eq(people.name, name));
    return entry[1]; 
end


local function onInitialise()
    --people_db = db:get_database("peopleinfo")
    --people = people_db.people;
end

--Exposed Variables and Functions
local peopleTracking = { Version = '0.1a' }

peopleTracking.SetCurrentHonours = function(who) currentHonours = who end
peopleTracking.GetCurrentHonours = function() return currentHonours end

peopleTracking.UpdateDetails = updateDetails
peopleTracking.GetDetails = getDetails

peopleTracking.OnInitialise = onInitialise;

Marvin.Events.RegisterEvent("InitPeopleTracking", "initialise", peopleTracking.OnInitialise)

return peopleTracking;
