--Thanks to Austere's Tracking Script
local affstrack = {}
local enemyState = {}

local haveDiagnoserBalance = true
local usingDiagnosing = true
local lastAffsAffected = {}

local recentAfflicted = {}
local recentCured = {}

local importantAfflictions = {"asthma", "slickness", "anorexia"}

MAX_HISTORY_SIZE = 50

local affInfo = {
   ["addiction"]      = { shortName = "add" },
   ["agoraphobia"]    = { shortName = "ag" },
   ["anorexia"]       = { shortName = "ano" },
   ["asthma"]         = { shortName = "ast"},
   ["blindness"]      = { shortName = "ubli"},
   ["bound"]          = { shortName = "bnd" },
   ["claustrophobia"] = { shortName = "cla" },
   ["clumsiness"]     = { shortName = "clu" },
   ["confusion"]      = { shortName = "con" },
   ["crippledarm"]    = { shortName = "cra" },
   ["crippledleg"]    = { shortName = "crl" },
   ["darkshade"]      = { shortName = "dar" },
   ["deadening"]      = { shortName = "dea" },
   ["deaf"]           = { shortName = "und" },
   ["dementia"]       = { shortName = "dem" },
   ["disloyalty"]     = { shortName = "dis" },
   ["disrupt"]        = { shortName = "disr" },
   ["dizziness"]      = { shortName = "diz" },
   ["epilepsy"]       = { shortName = "epi" },
   ["generosity"]     = { shortName = "gen" },
   ["haemophilia"]    = { shortName = "hae" },
   ["hallucinations"] = { shortName = "hal"  },
   ["healthleech"]    = { shortName = "health" },
   ["hypersomnia"]    = { shortName = "hyper" },
   ["hypochondria"]   = { shortName = "hypo" },
   ["illness"]        = { shortName = "ill" },
   ["impatience"]     = { shortName = "imp" },
   ["insomnia"]       = { shortName = "ins" },
   ["kola"]           = { shortName = "kol" },
   ["lethargy"]       = { shortName = "let" },
   ["loneliness"]     = { shortName = "lon" },
   ["masochism"]      = { shortName = "mas" },
   ["pacifism"]       = { shortName = "pac" },
   ["paralysis"]      = { shortName = "pa" },
   ["paranoia"]       = { shortName = "paran" },
   ["recklessness"]   = { shortName = "reck" },
   ["scytherus"]      = { shortName = "scy" },
   ["selarnia"]       = { shortName = "sel" },
   ["sensitivity"]    = { shortName = "sen" },
   ["shyness"]        = { shortName = "shy" },
   ["sleep"]          = { shortName = "sle" },
   ["slickness"]      = { shortName = "sli" },
   ["stupidity"]      = { shortName = "stu" },
   ["transfixed"]     = { shortName = "trans" },
   ["weariness"]      = { shortName = "wea" },
   ["vertigo"]        = { shortName = "ver" },
   ["voyria"]         = { shortName = "voy" },
   ["blind"]         =  { shortName = "bl" }
   
}

local lockAffs = {
  "paralysis", "weariness", "anorexia", "asthma", "slickness"
};

enemyTrackerHistory = {}
oldEnemyTrackerHistory = {}

function addToHistory(type, param)
  enemyTrackerHistory[#enemyTrackerHistory + 1] = {action = type, param = param}
  if(#enemyTrackerHistory > MAX_HISTORY_SIZE) then
    table.remove(enemyTrackerHistory, 1)
  end
end

function applyAction(type, param)
   if type == "processCure" then
      processCures(param.cureTable, false, param.curedAff, param.notCuredAffs)
   elseif type == "enemyEat" then onEnemyEat(param, false)
   elseif type == "enemySmoke" then onEnemySmoked(false)
   elseif type == "enemyApplied" then onEnemyApplied(param, false)
   elseif type == "enemyTreed" then onEnemyTreed( false)
   elseif type == "enemyFitness" then onEnemyFitness(false)
   elseif type == "enemyFocused" then onEnemyFocus(false)
   elseif type == "enemyCureAll" then onEnemyCureAll(false)
   elseif type == "addAff" then addAff(param)
   elseif type == "remAff" then remAff(param)
   end
end

function displayHistory()
  echo('\n')
  for k, v in ipairs(enemyTrackerHistory) do
    cecho("[EnemyTrackHistory] (" .. v.action .. ") - ")
    if(type(v.param) == "table") then
      display(v.param)
    else
      cecho(tostring(v.param))
    end
    echo('\n')
  end
  echo('\n')
  echo('\n')
--[[
  for k, v in ipairs(oldEnemyTrackerHistory) do
    cecho("[OldEnemyTrackHistory] (" .. v.action .. ") - ")
    if(type(v.param) == "table") then
      display(v.param)
    else
      cecho(tostring(v.param))
    end
    echo('\n')
  end
  --]]
end

function onConfirmAffliction(aff, notify)
  notify = notify or true
  --When an affliction is confirmed, check if we know 100% that we have it -
  --otherwise, our tracking is Offensive
  if(affstrack.score[aff] < 1.0) then
      --Tracking is off - go back through the history until we find where
      --the tracking was broken

      if notify then display("turns out we actually have " .. aff) end
      for i = #enemyTrackerHistory, 1, -1 do
        local hist = enemyTrackerHistory[i]
        if hist == nil then
          display(EnemyTrackHistory)
        end
        if(hist.action == "processCure" and table.contains(hist.param.cureTable, aff)) then
          local futureHistory = revertTrackingTo(i - 1); -- Revert to just before we ran this cure

          futureHistory[1].param.notCuredAffs = futureHistory[1].param.notCuredAffs  or {}
          futureHistory[1].param.notCuredAffs[#futureHistory[1].param.notCuredAffs + 1] = aff
          for _, cmd in pairs(futureHistory) do
             applyAction(cmd.action, cmd.param)
          end
        end
      end
  end
end

function onConfirmCured(aff)
  --When an affliction is cured, check if we know 100% that we have it -
  --otherwise, our tracking is Offensive
  if(affstrack.score[aff] ~= 0.0) then
      --Tracking is off - go back through the history until we find where
      --the tracking was broken
      display("turns out we don't have " .. aff)

      for i = #enemyTrackerHistory, 1, -1 do
        local hist = enemyTrackerHistory[i]
        if(hist.action == "processCure" and table.contains(hist.param.cureTable, aff)) then

          local futureHistory = revertTrackingTo(i - 1); -- Revert to just before we ran this cure

          --display(futureHistory)

          futureHistory[1].param.curedAff = aff;


          for _, cmd in pairs(futureHistory) do
             applyAction(cmd.action, cmd.param)
          end


          return
        end
        if(hist.action == "addAff" and hist.param == aff) then
              local futureHistory = revertTrackingTo(i); -- Revert to when we gave this affliction
              --Remove the aff
              applyAction("remAff", aff)
              --Now apply other effects
              for _, cmd in pairs(futureHistory) do
                 applyAction(cmd.action, cmd.param)
              end
              return
        end
      end
  end


end


function revertTrackingTo(location)
  oldEnemyTrackerHistory = {}
  local i = #enemyTrackerHistory
  while i > location do
    local hist = enemyTrackerHistory[i]
    if hist.action == "processCure"  then
      for aff, val in pairs(hist.param.oldValues) do
        affstrack.score[aff] = val
      end
    elseif hist.action == "addAff" then
        affstrack.score[hist.param] = 0.0
    end

    table.insert(oldEnemyTrackerHistory, 1, hist)
    i = i - 1
  end

  for i = location + 1, #enemyTrackerHistory do
    enemyTrackerHistory[i] = nil
  end

   return oldEnemyTrackerHistory;
end


function resetAffTrack()
  oldEnemyTrackerHistory = {}

    Marvin.Comms.echo("EnemyTrack", "Tracking reset")
    affstrack = {
          venoms = {
                  ["xentio"]     = "clumsiness",
                  ["eurypteria"] = "recklessness",
                  ["kalmia"]     = "asthma",
                  ["delphinium"] = "sleep",
                  ["digitalis"]  = "shyness",
                  ["darkshade"]  = "darkshade",
                  ["curare"]     = "paralysis",
                  ["epteth"]     = "crippledarm",
                  ["prefarar"]   = "sensitivity",
                  ["monkshood"]  = "disloyalty",
                  ["euphorbia"]  = "illness",
                  ["colocasia"]  = "deafblind",
                  ["vernalius"]  = "weariness",
                  ["epseth"]     = "crippledleg",
                  ["larkspur"]   = "dizziness",
                  ["slike"]      = "anorexia",
                  ["notechis"]   = "haemophilia",
                  ["vardrax"]    = "addiction",
                  ["aconite"]    = "stupidity",
                  ["selarnia"]   = "selarnia",
                  ["gecko"]      = "slickness",
                  ["scytherus"]  = "scytherus",
                  ["voyria"]     = "voyria",
                },

            applied = {
                  ["body"]  = {"anorexia", "ablaze"},
                  ["skin"]  = {"anorexia", "crippledarm", "crippledleg"},
                  ["torso"] = {"anorexia", "ablaze"},
                  ["arms"]  = {"crippledarm"},
                  ["legs"]  = {"crippledleg"}
                },

            eaten = {
                  ["piece of kelp"]    = {"asthma", "clumsiness", "hypochondria", "sensitivity", "weariness"},
                  ["aurum flake"]      = {"asthma", "clumsiness", "hypochondria", "sensitivity", "weariness"},
                  ["lobelia seed"]     = {"agoraphobia", "claustrophobia", "loneliness", "masochism", "recklessness", "vertigo"},
                  ["argentum flake"]   = {"agoraphobia", "claustrophobia", "loneliness", "masochism", "recklessness", "vertigo"},
                  ["prickly ash bark"] = {"confusion", "dementia", "hallucinations", "hypersomnia", "paranoia"},
                  ["stannum flake"]    = {"confusion", "dementia", "hallucinations", "hypersomnia", "paranoia"},
                  ["bellwort flower"]  = {"generosity", "pacifism"},
                  ["cuprum flake"]     = {"generosity", "pacifism"},
                  ["bayberry bark"]    = {"blind"},
                  ["arsenic pellet"]   = {"blind"},
                  ["hawthorn berry"]   = {"deaf"},
                  ["calamine crystal"]   = {"deaf"},
                  ["goldenseal root"]  = {"dizziness", "epilepsy", "impatience", "shyness", "stupidity"},
                  ["plumbum flake"]    = {"dizziness", "epilepsy", "impatience", "shyness", "stupidity"},
                  ["bloodroot leaf"]   = {"paralysis", "slickness"},
                  ["magnesium chip"]   = {"paralysis", "slickness"},
                  ["ginseng root"]     = {"addiction", "darkshade", "haemophilia", "lethargy", "illness", "scytherus"},
                  ["ferrum flake"]     = {"addiction", "darkshade", "haemophilia", "lethargy", "illness", "scytherus"}
                },

            focused = {
                  "agoraphobia",
                  "anorexia",
                  "claustrophobia",
                  "confusion",
                  "dizziness",
                  "epilepsy",
                  "generosity",
                  "loneliness",
                  "masochism",
                  "pacifism",
                  "recklessness",
                  "shyness",
                  "stupidity",
                  "weariness",
                  "vertigo",
                  "hallucinations"
                },

            treed = {
                  "clumsiness",
                  "paranoia",
                  "vertigo",
                  "agoraphobia",
                  "dizziness",
                  "claustrophobia",
                  "recklessness",
                  "epilepsy",
                  "addiction",
                  "stupidity",
                  "scytherus",
                  "slickness",
                  "generosity",
                  "pacifism",
                  "confusion",
                  "voyria",
                  "illness",
                  "haemophilia",
                  "weariness",
                  "hallucinations",
                  "confusion",
                  "disloyalty",
                  "lethargy",
                  "shyness",
                  "sensitivity",
                  "asthma",
                  "crippledarm",
                  "crippledleg",
                  "darkshade",
                  "impatience",
                  "anorexia",
                  "loneliness",
                  "hypochondria",
                  "selarnia",
                },

            restored = {
                  "crippledarm",
                  "crippledleg",
                },

            smoked = {
                  "deadening",
                  "disloyalty",
                  "slickness"
                },

            score = {
                  ["ablaze"]         = 0,
                  ["addiction"]      = 0,
                  ["agoraphobia"]    = 0,
                  ["anorexia"]       = 0,
                  ["asthma"]         = 0,
                  ["blind"]          = 0,
                  ["bound"]          = 0,
                  ["claustrophobia"] = 0,
                  ["clumsiness"]     = 0,
                  ["confusion"]      = 0,
                  ["crippledarm"]    = 0,
                  ["crippledleg"]    = 0,
                  ["darkshade"]      = 0,
                  ["deadening"]      = 0,
                  ["deaf"]           = 0,
                  ["dementia"]       = 0,
                  ["disloyalty"]     = 0,
                  ["disrupt"]        = 0,
                  ["dizziness"]      = 0,
                  ["epilepsy"]       = 0,
                  ["generosity"]     = 0,
                  ["haemophilia"]    = 0,
                  ["hallucinations"] = 0,
                  ["healthleech"]    = 0,
                  ["hypersomnia"]    = 0,
                  ["hypochondria"]   = 0,
                  ["illness"]        = 0,
                  ["impatience"]     = 0,
                  ["insomnia"]       = 0,
                  ["kola"]           = 0,
                  ["lethargy"]       = 0,
                  ["loneliness"]     = 0,
                  ["masochism"]      = 0,
                  ["pacifism"]       = 0,
                  ["paralysis"]      = 0,
                  ["paranoia"]       = 0,
                  ["recklessness"]   = 0,
                  ["scytherus"]      = 0,
                  ["selarnia"]       = 0,
                  ["sensitivity"]    = 0,
                  ["shyness"]        = 0,
                  ["sleep"]          = 0,
                  ["slickness"]      = 0,
                  ["stupidity"]      = 0,
                  ["transfixed"]     = 0,
                  ["weariness"]      = 0,
                  ["vertigo"]        = 0,
                  ["voyria"]         = 0
                },
            cureall = {
                  "addiction",
                  "agoraphobia",
                  "anorexia",
                  "asthma",
                  "claustrophobia",
                  "clumsiness",
                  "confusion",
                  "crippledarm",
                  "crippledleg",
                  "darkshade",
                  "deadening",
                  "dementia",
                  "disloyalty",
                  "disrupt",
                  "dizziness",
                  "epilepsy",
                  "generosity",
                  "haemophilia",
                  "hallucinations",
                  "hypersomnia",
                  "hypochondria",
                  "illness",
                  "impatience",
                  "lethargy",
                  "loneliness",
                  "masochism",
                  "pacifism",
                  "paralysis",
                  "paranoia",
                  "recklessness",
                  "scytherus",
                  "selarnia",
                  "sensitivity",
                  "shyness",
                  "slickness",
                  "stupidity",
                  "weariness",
                  "vertigo",
                  "voyria",
                },
            }

            enemyState = {
               prone = false;
               rebounding = true;
               usesRebounding = false;
               shield = false;
               herbBalance = true;
               herbBalance_trig = nil;
               engaged = false;
            }

            enemyTrackerHistory = {}
end


function addAff(newAff, announce)
  if(not affstrack.score[newAff]) then
    return
  end

  if newAff == "sensitivity" then
     if(affstrack.score.deaf == 0.0) then
        affstrack.score.deaf = 1.0
        addToHistory("addAff", "deaf");
        recentAfflicted[#recentAfflicted + 1] = "deaf"
        if (announce) then Marvin.Comms.echo("EnemyTrack", "Afflicted with: <green>deaf") end
      else
         affstrack.score.sensitivity = 1.0
         addToHistory("addAff", "sensitivity");
         recentAfflicted[#recentAfflicted + 1] = "sensitivity"
         if announce then Marvin.Comms.echo("EnemyTrack", "Afflicted with: <green>" .. newAff) end
      end
   else
      affstrack.score[newAff] = 1.0;
      addToHistory("addAff", newAff);
      recentAfflicted[#recentAfflicted + 1] = newAff
      if announce then Marvin.Comms.echo("EnemyTrack", "Afflicted with: <green>" .. newAff) end
   end

   Marvin.Events.RaiseEvent("enemyGainedAff")
end

function remAff(lostAff)
    addToHistory("remAff", lostAff)
    affstrack.score[lostAff] = 0.0
end

function removeLastAff()
  local lastAction = enemyTrackerHistory[#enemyTrackerHistory]
  if lastAction.action == "addAff" then
    affstrack.score[lastAction.param] = 0.0
    enemyTrackerHistory[#enemyTrackerHistory] = nil
  end
end

function onEnemyFocus(shouldProcess)
  if (affstrack.score.impatience > 0.0) then
    onConfirmCured("impatience")
  end

  addToHistory("enemyFocus")
  shouldProcess = shouldProcess or true
  if shouldProcess then processCures(affstrack.focused) end
end

function onEnemyDrink(shouldProcess)
  if (affstrack.score.anorexia > 0.0) then
    onConfirmCured("anorexia")
  end

  addToHistory("enemyDrink")
end


function setdeafOff()
  remAff("deaf")
end

function onEnemyDragonHeal(shouldProcess)
  --Todo add weariness/reckless confirm cured

  addToHistory("enemyDragonheal") 
  shouldProcess = shouldProcess or false
  if shouldProcess then processDragonheal() end
end

function onEnemyEat(herb, shouldProcess )
    if (affstrack.score.anorexia > 0.0) then
        onConfirmCured("anorexia")
    end

    if(enemyState.herbBalance == false) then return end --Testing

    if(affstrack.eaten[herb]) then addToHistory("enemyEat", herb) end

    shouldProcess = shouldProcess or true
    if not shouldProcess then return end

    if herb == "calamine crystal" or herb == "hawthorn berry" then
        if(affstrack.score.deaf > 0.0) then
        tempTimer(2.5,
            function()
                setdeafOff();
                recentCured[#recentCured + 1] = "deaf";
            end)
        end 
    else
        if(affstrack.eaten[herb] ) then
          processCures(affstrack.eaten[herb])
      end
   end


   if enemyState.herbBalance_trig then
      killTimer(enemyState.herbBalance_trig)
   end

   enemyState.herbBalance = false;
   enemyState.herbBalance_trig = tempTimer(1.0, [[Marvin.Tracking.EnemyTracking.ResetHerbBalance()]]);
end

function onEnemySmoked(shouldProcess)
   if (affstrack.score.asthma > 0.0) then
      onConfirmCured("asthma")
   end

   addToHistory("enemySmoked")
   shouldProcess = shouldProcess or true
   if shouldProcess then processCures(affstrack.smoked) end
end

function onEnemyApplied(limb, shouldProcess)
   if (affstrack.score.slickness > 0.0) then
      onConfirmCured("slickness")
   end

   addToHistory("enemyApplied", limb)
   shouldProcess = shouldProcess or true
   if (shouldProcess and affstrack.applied[limb]) then processCures(affstrack.applied[limb]) end
end

function onEnemyTreed(shouldProcess)
   if (affstrack.score.paralysis > 0.0) then
      onConfirmCured("paralysis")
   end

   addToHistory("enemyTreed")
   shouldProcess = shouldProcess or true
   if shouldProcess then processCures(affstrack.treed) end

end

function onEnemyRestored(shouldProcess)
   addToHistory("enemyRestored")
   shouldProcess = shouldProcess or true
   if shouldProcess then processCures(affstrack.restored) end
end

function onEnemyFitness()
  addToHistory("enemeyFitnessed")
  remAff("asthma")
end

function onEnemyCureAll(shouldProcess)
  addToHistory("enemeyFitnessed")
  shouldProcess = shouldProcess or true
  if shouldProcess then processCures(affstrack.cureall) end
end

function processCures(cureTable, announce, cured, notCured)

   local affs = {}
   for _, aff in ipairs(cureTable) do
      if(affstrack.score[aff] > 0) then
         affs[#affs + 1] = aff;
     end
   end

   if(usingDiagnosing == false and haveDiagnoserBalance) then lastAffsAffected = { } end


   if(#affs > 0) then
      local oldAffValues = {}

      for _, aff in ipairs(affs) do
            if(usingDiagnosing == false and haveDiagnoserBalance) then lastAffsAffected[aff] = affstrack.score[aff] end

            oldAffValues[aff] = affstrack.score[aff]

            if(#affs == 1 or cured == aff) then
              --onConfirmAffliction(aff)
              affstrack.score[aff] = 0.0
            else
              if not (cured or notCured) then
                 local cureChance = 1 - (1 / #affs)
                 affstrack.score[aff] = affstrack.score[aff] * cureChance
              else
                  if table.contains(notCured, aff) then
                    --affstrack.score[aff] = 1
                  else
                    local numAffs = #affs
                    if notCured then
                      numAffs = #affs - #notCured
                    end

                    if(cured and numAffs == 2) then
                     --Do nothing - affliction should stay at same level
                    else
                      local cureChance = 1 - (1 / numAffs)
                      affstrack.score[aff] = affstrack.score[aff] * cureChance
                    end
                  end
              end
           end

            recentCured[#recentCured + 1] = aff;

            if(affstrack.score[aff] < 0.1) then
               affstrack.score[aff] = 0.0
            end
      end

      --showAffsFull()

      addToHistory("processCure", {cureTable = cureTable, oldValues = oldAffValues, curedAff = cured, notCuredAffs = notCured})

      if(announce) then
         Marvin.Comms.echo("EnemyTrack", "Might have cured: <red>" .. table.concat(affs, " "))
      end
      --lastAffsAffected = affs
      if(#affs > 1 and haveDiagnoserBalance and not usingDiagnosing) then
         doDiagnoser()
      end
    end

    Marvin.Events.RaiseEvent("enemyAffProcessCures")
end

function processDragonheal()
   local affs = {}
   for _, aff in ipairs(cureTable) do
      if(affstrack.score[aff] > 0) then
         affs[#affs + 1] = aff;
     end
   end

   for _, aff in ipairs(affs) do
      recentCured[#recentCured + 1] = aff;
      affstrack.score[aff] = 0.0
    end
end

function onVenomHit(venom)
  addAff(affstrack.venoms[venom]);
end

function doDiagnoser()
  if usingDiagnoser or not haveDiagnoserBalance then return end

  for _, aff in pairs(importantAfflictions) do
    if affstrack.score[aff] > 0.0 and affstrack.score[aff] < 1.0 then
      send("check " .. Marvin.Offensive.Targetting.GetTarget() .. " for " .. aff);
      usingDiagnoser = true
      if resetDiagTimer then
         killTimer(resetDiagTimer)
      end
      resetDiagTimer = tempTimer(2, function() usingDiagnoser = false end)
      return
    end
  end
end

function onDiagnoserBalance()
   haveDiagnoserBalance = true;
   doDiagnoser();
end

function onSuccessfulDiagnose(enemyAff)

   if resetDiagTimer then
      killTimer(resetDiagTimer)
   end

  onConfirmAffliction(enemyAff);

   usingDiagnoser = false;
   haveDiagnoserBalance = false;
end

function onFailedDiagnose(enemyAff)

  if resetDiagTimer then
      killTimer(resetDiagTimer)
  end

   --We don't have the affliction, so the others were not cured
  onConfirmCured(enemyAff)

   usingDiagnoser = false;
   haveDiagnoserBalance = false;
end
--Display functions

local function removeDuplicates(t)
  local flags = {}
  local tn = {}
  for i=1,table.getn(t)  do
    if not flags[t[i]] then
        table.insert(tn, t[i])
        flags[t[i]] = true
     end
  end

  return tn
end

function onShowAffChanges()
   if(#recentCured > 0) then
      recentCured = removeDuplicates(recentCured)
      if(#recentCured == 1) then
         Marvin.Comms.echo("EnemyTrack", "Cured <red>" .. recentCured[1])
      else
         Marvin.Comms.echo("EnemyTrack", "Might have cured: <red>" .. table.concat(recentCured, " "))
      end
      recentCured = {}
      Marvin.Offensive.Attacking.BuildAttack()
   end
   if(#recentAfflicted > 0) then
      recentAfflicted = removeDuplicates(recentAfflicted)
      Marvin.Events.RaiseEvent("afflictionsGiven", recentAfflicted);
      Marvin.Comms.echo("EnemyTrack", "Afflicted with: <green>" .. table.concat(recentAfflicted, " "))
      recentAfflicted = {}
      Marvin.Offensive.Attacking.BuildAttack()
   end

   --For now..
end


function showAffs()
  local noAffs = true
  for aff, val in pairs(affstrack.score) do
    if val > 0.3 then
      noAffs = false
      break
    end
  end

  if noAffs then return "" end

	local cmd = "\n<grey>["

  --Affliction affs should be first
  for _, aff in ipairs(lockAffs) do
    local score = affstrack.score[aff]
    if score > 0.5 then
      cmd = cmd .. "<deep_sky_blue>" .. affInfo[aff].shortName .. " "
    elseif score > 0.3 then
      cmd = cmd .. "<steel_blue>" .. affInfo[aff].shortName .. " "
    end
  end

  --Then illness
  local score = affstrack.score.illness
  if score > 0.5 then
    cmd = cmd .. "<green>" .. affInfo.illness.shortName .. " "
  elseif score > 0.3 then
    cmd = cmd .. "<sea_green>" .. affInfo.illness.shortName .. " "
  end

  --Then everything else
  for aff, val in pairs(affstrack.score) do
    if (table.contains(lockAffs, aff) or (aff == "illness")) then
    else
		    if (val > 0.3) then
          local ai = affInfo[aff]
          if not ai then display(aff) end
          local c;
          if val > 0.5 then
            c = "firebrick"
          else
            c = "coral"
          end
          cmd = cmd .. "<" .. c .. ">"
          cmd = cmd .. ai.shortName;
          cmd = cmd .. " "
        end
    end
	end

	cmd = cmd .. "<grey>]";
	return cmd;
end

function setEnemyState(state, value)
   enemyState[state] = value;
end

function getEnemyState(state)
   return enemyState[state] or false
end

function showAffsFull()
  	for aff, val in pairs(affstrack.score) do
      if val > 0.0 then
        cecho("<red>" .. aff .. ": <white>" .. val .. "\n")
      end
    end
end

function getLabelAffs()
  local colouredAffs = {
    impatience = "lime",
    epilepsy = "lime",
    hallucinations = "orangered",
    dizziness = "orangered",
    recklessness = "orangered",
    confusion = "orangered",
    paranoia = "orangered"
  }

  local resetFontToWhite = [[<font color="white">]]
  local txt = resetFontToWhite .. "[ "
  local affCount = 0

  for aff, val in pairs(affstrack.score) do
    if (val > 0.3) then
      local ai = affInfo[aff]
      local colour = colouredAffs[aff] or "grey"
      local fontColour = [[<font color="]] .. colour .. [[">]]
      txt = txt .. fontColour .. ai.shortName .. resetFontToWhite .. " "
      affCount = affCount + 1
    end
  end
  
  txt = txt .. "]"
  
  if(affCount > 0) then
    return txt;
  else
    return ""
  end
end


local function registerCustomPrompt()
  svo.adddefinition("@enemyAffs", "Marvin.Tracking.EnemyTracking.ShowAffs()")
end

--Exposed Variables and Functions
local enemyTracking = { Version = '0.1b' }

enemyTracking.AddAff = addAff;
enemyTracking.OnEnemyFocus = onEnemyFocus;
enemyTracking.OnEnemyDrink = onEnemyDrink;
enemyTracking.OnEnemyEat = onEnemyEat;
enemyTracking.OnEnemySmoked = onEnemySmoked;
enemyTracking.OnEnemyApplied = onEnemyApplied;
enemyTracking.OnEnemyTreed = onEnemyTreed;
enemyTracking.OnEnemyRestored = onEnemyRestored;
enemyTracking.OnEnemyFitness = onEnemyFitness;
enemyTracking.OnEnemyCureAll = onEnemyCureAll;
enemyTracking.onEnemyDragonHeal = onEnemyDragonHeal;

enemyTracking.OnClassHit = onClassHit;
enemyTracking.OnVenomHit = onVenomHit;

enemyTracking.RemoveLastAff = removeLastAff;

enemyTracking.GetEnemyAffs = function() return affstrack.score end
enemyTracking.GetEnemyStateTable = function() return enemyState end

enemyTracking.SetEnemyState = setEnemyState;
enemyTracking.GetEnemyState = getEnemyState;

enemyTracking.ShowAffs = showAffs;

enemyTracking.OnDiagnoserBalance = onDiagnoserBalance;
enemyTracking.OnSuccessfulDiagnose = onSuccessfulDiagnose;
enemyTracking.OnFailedDiagnose = onFailedDiagnose

enemyTracking.OnShowAffChanges = onShowAffChanges;

enemyTracking.ResetTracking = resetAffTrack;

enemyTracking.DisplayHistory = displayHistory;

enemyTracking.OnConfirmCured = onConfirmCured;
enemyTracking.OnConfirmAffliction = onConfirmAffliction;

enemyTracking.ShowAffsFull = showAffsFull;
enemyTracking.GetLabelAffs = getLabelAffs;

enemyTracking.RevertTrackingTo = revertTrackingTo
enemyTracking.ResetHerbBalance = function() enemyState.herbBalance = true end

enemyTracking.RegisterCustomPrompt = registerCustomPrompt;

Marvin.Events.RegisterEvent("displayEnemyAffChange", "prompt", enemyTracking.OnShowAffChanges)

resetAffTrack();
--wsys.prompttags.showAffs = enemyTracking.ShowAffs;
if svo then
  enemyTracking.RegisterCustomPrompt();
else
  registerAnonymousEventHandler("svo system loaded", "enemyTracking.RegisterCustomPrompt")
end


return enemyTracking
