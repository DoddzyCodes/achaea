local limbBreakPoints = {}
table.insert(limbBreakPoints, {health = 4044, hits = 5});
table.insert(limbBreakPoints, {health = 4923, hits = 6});
table.insert(limbBreakPoints, {health = 5504, hits = 6});
table.insert(limbBreakPoints, {health = 5658, hits = 6});
table.insert(limbBreakPoints, {health = 6234, hits = 7});
table.insert(limbBreakPoints, {health = 8250, hits = 8});

-- 6234 6 + 1 trip
local limbDamage = {
    head = 0,
    torso = 0,
    leftArm = 0,
    rightArm = 0,
    leftLeg = 0,
    rightLeg = 0
}

local limbHitAttempt = nil;


function resetDamage()
    limbDamage = {
        head = 0,
        torso = 0,
        leftArm = 0,
        rightArm = 0,
        leftLeg = 0,
        rightLeg = 0
    }

    
    Marvin.Comms.echo("Limb Tracker", "<light_grey>Enemy limb counting reset!")
	Marvin.Events.RaiseEvent("targetLimbHit");

end

local currentBreakPoint = 10

local currentLimbTarget = "nothing"

function onAttemptedHit(type, limb)
    local limbFix = {
        ["left arm"] = "leftArm",
        ["right arm"] = "rightArm",
        ["left leg"] = "leftLeg",
        ["right leg"] = "rightLeg",
    }
    limb = limbFix[limb] or limb

    limbHitAttempt = {
        ["type"] = type,
        ["limb"] = limb
    }

    enableTrigger("LimbTrackMiss")
end

function onAttackMissed()
    limbHitAttempt = nil
    disableTrigger("LimbTrackMiss")
end

function onPrompt()
    if limbHitAttempt then
        addLimbHit(limbHitAttempt.limb, 1)
        limbHitAttempt = nil
        disableTrigger("LimbTrackMiss")
    end
end


function onSalveTick(limb)
    if limbDamage[limb] >= currentBreakPoint then
        limbDamage[limb] = 0
        Marvin.Comms.echo("Limb Tracker", "<blue>Enemy limb no longer broken")
	    Marvin.Events.RaiseEvent("targetLimbHit", limbDamage[limb]);
    end
end

function onTargetSalveApply(limb)
    if limbDamage[limb] then
        if limbDamage[limb] >= currentBreakPoint then
            tempTimer(4.0, [[ Marvin.Tracking.LimbTracking.OnSalveTick("]] .. limb .. [[") ]])
        end
    else
        if limb == "arms" then
            if limbDamage["leftArm"] >= currentBreakPoint then
                tempTimer(4.0, [[ Marvin.Tracking.LimbTracking.OnSalveTick("leftArm") ]])
            elseif limbDamage["rightArm"] >= currentBreakPoint then
                tempTimer(4.0, [[ Marvin.Tracking.LimbTracking.OnSalveTick("rightArm") ]])
            end
        else
            if limbDamage["leftLeg"] >= currentBreakPoint then
                tempTimer(4.0, [[ Marvin.Tracking.LimbTracking.OnSalveTick("leftLeg") ]])
            elseif limbDamage["rightLeg"] >= currentBreakPoint then
                tempTimer(4.0, [[ Marvin.Tracking.LimbTracking.OnSalveTick("rightLeg") ]])
            end 
        end
    end

end

function addLimbHit(limb, number)
    limbDamage[limb] = limbDamage[limb] + number;
    if limbDamage[limb] >= currentBreakPoint then
        Marvin.Comms.echo("Limb Tracker", "<red>Enemy limb broken! - Enemy limb broken!")
        Marvin.Comms.echo("Limb Tracker", "<red>Enemy limb broken! - Enemy limb broken!")
    elseif limbDamage[limb] == (currentBreakPoint - 1) then
        Marvin.Comms.echo("Limb Tracker", "<yellow>Enemy limb prepped - one hit to break")
        Marvin.Comms.echo("Limb Tracker", "<yellow>Enemy limb prepped - one hit to break")
    end

	Marvin.Events.RaiseEvent("targetLimbHit", limbDamage[limb]);
end

function setBreakpoint(numHits)
    currentBreakPoint = numHits;
    Marvin.Comms.echo("Limb Tracker", "<light_grey>Enemy limbs registered to break at <red>" .. numHits .. "<light_grey> hits.")
    
	Marvin.Events.RaiseEvent("targetLimbBreakpointChange", limbDamage[limb]);
end

function findTargetBreakPoint(health, autoset)
    local breakPoint = 10;
    for _, info in ipairs(limbBreakPoints) do
        if(info.health >= health) then
            breakPoint = info.hits
            break;
        end
    end

    Marvin.Comms.echo("Limb Tracker", "<light_grey>Limbs will take <red>" .. breakPoint .. "<light_grey> hits to break.")
    if autoset then
        setBreakpoint(breakPoint)
    end
end

function setCurrentLimbTarget(limb)
    currentLimbTarget = limb
    Marvin.Events.RaiseEvent("targetLimbChanged")
end

function getCurrentLimbTarget()
    return currentLimbTarget;
end

function getLabelColorString(limb)
    local str = [[<font color="]]

    if limbDamage[limb] >= currentBreakPoint then
        str = str .. "red"
    elseif limbDamage[limb] == (currentBreakPoint - 1) then
        str = str .. "yellow"
    else
        str = str .. "lightsteelblue"
    end

    str = str .. [[">]]
    return str;
end

function getLabelLimbTarget()
    local txt = currentLimbTarget;

    return txt;
end

function getLabelAffs()
    local returnToWhite = [[<font color="white">]]
    local txt = "[ " .. getLabelColorString("head") .. limbDamage.head
    txt = txt .. returnToWhite .. "-"
    txt = txt .. getLabelColorString("torso") .. limbDamage.torso
    txt = txt .. returnToWhite .. " | "
    txt = txt .. getLabelColorString("leftArm") .. limbDamage.leftArm
    txt = txt .. returnToWhite .. "-"
    txt = txt .. getLabelColorString("rightArm") .. limbDamage.rightArm
    txt = txt .. returnToWhite .. " | " 
    txt = txt .. getLabelColorString("leftLeg") .. limbDamage.leftLeg
    txt = txt .. returnToWhite .. "-"
    txt = txt .. getLabelColorString("rightLeg") .. limbDamage.rightLeg
    txt = txt .. returnToWhite .. " ]"
    return txt
end

--Exposed Variables and Functions
local LimbTracking = { Version = '0.1a' }


LimbTracking.OnAttemptedHit = onAttemptedHit;
LimbTracking.OnAttackMissed = onAttackMissed;
LimbTracking.OnSalveTick = onSalveTick;
LimbTracking.OnTargetSalveApply = onTargetSalveApply;

LimbTracking.AddLimbHit = addLimbHit;

LimbTracking.ResetDamage = resetDamage;
LimbTracking.SetBreakpoint = setBreakpoint;
LimbTracking.FindTargetBreakPoint = findTargetBreakPoint;

LimbTracking.SetCurrentLimbTarget = setCurrentLimbTarget;
LimbTracking.GetCurrentLimbTarget = getCurrentLimbTarget;

LimbTracking.getLabelLimbTarget = getLabelLimbTarget;
LimbTracking.GetLabelAffs = getLabelAffs;


Marvin.Events.RegisterEvent(onPrompt, "prompt", onPrompt);
return LimbTracking;