local enemyDataHeight = 40
local enemyDataContainer = enemyDataContainer or {}
local enemyDataPanel = enemyDataPanel or {}
local enemyDataSeperator = enemyDataSeperator or {}

function setupUI()
	setBorderBottom(enemyDataHeight)
	enemyDataContainer = Geyser.Container:new({
  		name = "EnemyData",    -- give it a unique name here
  		x=0, y=-enemyDataHeight,                   -- have it start at the top-left corner of mudlet
  		width="100%", height=enemyDataHeight, -- with a width of 200, and a height of the full screen, hence 100%
	})

	enemyDataSeperator = Geyser.Label:new({
  		name="EnemyData_Seperator",
  		x=0, y=0,
  		width="100%", height=enemyDataHeight
	}, enemyDataContainer)
	enemyDataSeperator:setStyleSheet([[
  		background-color: rgb(255,0,0);
	]])

	enemyDataPanel = Geyser.Label:new({
  		name="EnemyData_InfoPanel",
  		x=0, y=3,
  		width="100%", height=enemyDataHeight-3
	}, enemyDataContainer)
	enemyDataPanel:setStyleSheet([[
  		background-color: rgb(0,0,0);
	]])
	
end

function clearUI()
	setBorderBottom(0)
	enemyDataContainer:hide();
end

function updateEnemyData()
	local target = Marvin.Offensive.Targetting.GetTarget();
	local enemyAffs = Marvin.Tracking.EnemyTracking.GetLabelAffs();
	local limbs = Marvin.Tracking.LimbTracking.GetLabelAffs();
	local limbTarget = Marvin.Tracking.LimbTracking.getLabelLimbTarget();

	enemyDataPanel:echo([[
		<p style="font-size:15px">T: ]] .. target .. [[ | ]] ..  limbTarget .. [[ | ]] .. enemyAffs .. [[<br>]] .. limbs)
end

function updateSeperator()
	if svo.newbals.balance and svo.newbals.equilibrium then
		enemyDataSeperator:setStyleSheet([[
  		background-color: rgb(0,255,0);
	]])
	else
		enemyDataSeperator:setStyleSheet([[
  		background-color: rgb(255,0,0);
	]])
	end
end


local WindowLayout = {Version = "0.1a"}

WindowLayout.UpdateEnemyData = updateEnemyData
WindowLayout.UpdateSeperator = updateSeperator

Marvin.Events.RegisterEvent("WindowLayout_OnChangeTarget", "targetChanged", WindowLayout.UpdateEnemyData)
Marvin.Events.RegisterEvent("WindowLayout_OnTargetBreakpointChanged", "targetLimbBreakpointChange", WindowLayout.UpdateEnemyData)
Marvin.Events.RegisterEvent("WindowLayout_OnTargettingLimbChanged", "targetLimbChanged", WindowLayout.UpdateEnemyData)
Marvin.Events.RegisterEvent("WindowLayout_OnTargetLimbHit", "targetLimbHit", WindowLayout.UpdateEnemyData)
Marvin.Events.RegisterEvent("WindowLayout_OnEnemyGainedAff", "enemyGainedAff", WindowLayout.UpdateEnemyData)
Marvin.Events.RegisterEvent("WindowLayout_OnEnemyProcessCures", "enemyAffProcessCures", WindowLayout.UpdateEnemyData)
Marvin.Events.RegisterEvent("WindowLayout_OnUpdateSeperator", "prompt", WindowLayout.UpdateSeperator);

setupUI();
updateEnemyData();
return WindowLayout;