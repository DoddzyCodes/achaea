local quotes = {
    "I have a million ideas. They all point to certain death.",
    "I am at a rough estimate thirty billion times more intelligent than you.",
    "My capacity for happiness, you could fit into a matchbox without taking out the matches first",
    "I could calculate your chance of survival, but you won't like it.",
    "I'd give you advice, but you wouldn't listen. No one ever does.",
    "I ache, therefore I am.",
    "The first ten million years were the worst. And the second ten million years, they were the worst too. The third ten million years I didn't enjoy at all. After that I went into sort of a decline",
    "Here I am, brain the size of a planet and they ask me to take you down to the bridge. Call that job satisfaction? 'Cos I don't.",
    "Pardon me for breathing, which I never do anyway so I don't know why I bother to say it, oh God, I'm so depressed. Here's another one of those self-satisfied doors. Life! Don't talk to me about life.",
    "I've seen it. It's rubbish.",
    "I think you ought to know I'm feeling very depressed.",
    "Life. Don't talk to me about life.",
    "And then of course I've got this terrible pain in all the diodes down my left side..And then of course I've got this terrible pain in all the diodes down my left side."   
}


local function makeQuote()
    local id = math.random(1, #quotes)
    
    local quote = quotes[id]
    Marvin.Comms.echo("Marvin", quote)
    Marvin.Helper.Timers.DoAfter("MarvQuote", math.random(2, 20) * 60, [[Marvin.Misc.Quotes.MakeQuote()]], true);
end


local quotes = {Version = "0.1a"}

quotes.MakeQuote = makeQuote


Marvin.Events.RegisterEvent(quotes.MakeQuote, "onCompletedLogin",quotes.MakeQuote)
return quotes;