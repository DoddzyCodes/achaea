local systemTable =  {
	startAttack = function()
        class = Marvin.GMCP.CharStatus.Class():title();
		if keneanung.bashing.attacking > 0 then
            if keneanung.bashing.systems.marvin.queueTrigger then keneanung.bashing.systems.marvin.setup() end
			enableTrigger(keneanung.bashing.systems.marvin.queueTrigger)
			local command
			if keneanung.bashing.configuration[class].attackcommand:find("&tar") then
				command = keneanung.bashing.configuration[class].attackcommand
			else
				command = keneanung.bashing.configuration[class].attackcommand .. " &tar"
			end
			Marvin.ServerQueue.AddCommand(command, true)
 	 	end
	end,
	
	stopAttack = function()
		disableTrigger(keneanung.bashing.systems.marvin.queueTrigger)
	    Marvin.ServerQueue.ClearQueue()
	end,
	
	flee = function()
		keneanung.bashing.systems.marvin.stopAttack()
		Marvin.ServerQueue.AddCommand(keneanung.bashing.fleeDirection, false, true);
	end,
	
	warnFlee = function(avg)
		svo.boxDisplay("Better run or get ready to die!", "orange")
	end,
	
	notifyFlee = function(avg)
		svo.boxDisplay("Running as you have not enough health left.", "red")
	end,

	handleShield = function()
        class = Marvin.GMCP.CharStatus.Class():title();
		if keneanung.bashing.configuration[class].autoraze then
			local command
			if keneanung.bashing.configuration[class].razecommand:find("&tar") then
				command = keneanung.bashing.configuration[class].razecommand
			else
				command = keneanung.bashing.configuration[class].razecommand .. " &tar"
			end
			Marvin.ServerQueue.AddCommandFirst(command)
		end
		keneanung.bashing.shield = true
	end,

	brokeShield = function()
		--wsys.undo(true, 1)
	end,
	
	setup = function()
		keneanung.bashing.systems.marvin.queueTrigger = tempTrigger("[System]: Running queued eqbal command: SERVERDO",
			[[
			local system = keneanung.bashing.systems[keneanung.bashing.configuration.system]
			keneanung.bashing.attacks = keneanung.bashing.attacks + 1
			local avgDmg = keneanung.bashing.damage / keneanung.bashing.attacks
			local avgHeal = keneanung.bashing.healing / keneanung.bashing.attacks
			
			local estimatedDmg = avgDmg * 2 - avgHeal
			local fleeat = keneanung.bashing.calcFleeValue(keneanung.bashing.configuration.fleeing)
			local warnat = keneanung.bashing.calcFleeValue(keneanung.bashing.configuration.warning)
			if estimatedDmg > gmcp.Char.Vitals.hp - fleeat and keneanung.bashing.configuration.autoflee then
				system.notifyFlee(estimatedDmg)
				system.flee()
			else
				if estimatedDmg > gmcp.Char.Vitals.hp - warnat then
					system.warnFlee(estimatedDmg)
				end
			end
			]])
		disableTrigger(keneanung.bashing.systems.marvin.queueTrigger)
		registerAnonymousEventHandler("do action run", "keneanung.bashing.systems.marvin.doActionRun")
	end,
	
	teardown = function()
		if keneanung.bashing.systems.marvin.queueTrigger then
			killTrigger(keneanung.bashing.systems.marvin.queueTrigger)
		end
	end,

	doActionRun = function(_, command)
		local razecommand
        class = Marvin.GMCP.CharStatus.Class():title();
		if keneanung.bashing.configuration[class].razecommand:find("&tar") then
			razecommand = keneanung.bashing.configuration[class].razecommand
		else
			razecommand = keneanung.bashing.configuration[class].razecommand .. " &tar"
		end
		if command == razecommand then
			keneanung.bashing.shield = false
		end
	end,

	pause = function()
		keneanung.bashing.systems.marvin.stopAttack()
	end,

	unpause = function()
		keneanung.bashing.systems.marvin.startAttack()
	end
}

keneanung.bashing.systems.marvin = systemTable;


local Hunting = {Version = "0.1a"}

return Hunting