
local currentCuringProfile = currentCuringProfile or "default"
local currentPrios = currentPrios or {}
local switchedPrios = switchedPrios or {}
local autoUpdatedPriorities = autoUpdatedPriorities or {}

local AffTracking = Marvin.Defensive.AfflictionTracking;

local priorityUpdates = {
  --[[x
    sensitivityOnProne = {
        check = function()
            if wsys.aff.prone then
                return true;
            else
                return false;
            end
        end,
        onRequirementsMet = function()
            Marvin.Defensive.PrioSwitching.SwitchAfflictionPriority("sensitivity", 1);
        end,
        onRequiredmentsCured = function()
             Marvin.Defensive.PrioSwitching.ResetAfflictionPriority("sensitivity");
        end
    } ]]
}


local function switchAffPrio(aff, newPrio)
    wsys.switchPrios(aff, newPrio)
end

local function resetAffPrio(aff)
    wsys.affPrioRestore( aff )
end

local function checkAutoUpdatedPrios()
    for name, info in pairs(autoUpdatedPriorities) do
        if not info.check() then
            info.onRequiredmentsCured()
            autoUpdatedPriorities[name] = nil
        end
    end
    
    for name, info in pairs(priorityUpdates) do
        if not autoUpdatedPriorities[name] and info.check() then
            info.onRequirementsMet();
            autoUpdatedPriorities[name] = info
        end
    end
end

local function onAfflictionGained()
    checkAutoUpdatedPrios()
end

local function onAfflictionLost()
    checkAutoUpdatedPrios()
end

local function onGMCPAfflictionAdd()
    onAfflictionGained()
end

local function onGMCPAfflictionRemove()
    onAfflictionLost()
end



local PrioSwitching = { Version = '0.1a' }

PrioSwitching.SwitchAfflictionPriority = switchAffPrio;
PrioSwitching.ResetAfflictionPriority = resetAffPrio;

PrioSwitching.OnGMCPAfflictionAdd = onGMCPAfflictionAdd;
PrioSwitching.OnGMCPAfflictionRemove = onGMCPAfflictionRemove;

registerAnonymousEventHandler("gmcp.Char.Afflictions.Add", "Marvin.Defensive.PrioSwitching.OnGMCPAfflictionAdd")
registerAnonymousEventHandler("gmcp.Char.Afflictions.Remove", "Marvin.Defensive.PrioSwitching.OnGMCPAfflictionRemove")


return PrioSwitching

