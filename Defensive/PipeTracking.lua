local skullcapPipe = nil
local valerianPipe = nil
local elmPipe = nil

local function checkForPipeInContainer(loc, item)
    --display(loc .. " " .. item.id)
    if item.name:find("pipe") and (loc ~= "inv" and loc ~= "room") then
        send("queue prepend eqbal get " .. item.id .. " from " .. loc:sub(4))
    end

end

local PipeTracking = { Version = '0.1a' }

PipeTracking.CheckForPipeInContainer = checkForPipeInContainer

Marvin.Events.RegisterEvent(PipeTracking.CheckForPipeInContainer, "onItemAdded", PipeTracking.CheckForPipeInContainer)
return PipeTracking
