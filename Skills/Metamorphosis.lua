local currentMorph = "human"

local powers = {
	bite = {command = "bite @target", morphs = {"basilisk"}},
	burrow = {command = "burrow @1", morphs = {"wolverine"}},
	digging = {command = "dig", morphs = {"wolverine"}},
	claw = {command = "claw @target", morphs = {"wolverine"}},
	alertness = {command = "alertness on", morphs = {"icewyrm", "jaguar"}},
	ambush = {command = "ambush @target", morphs = {"jaguar", "wolverine"}},
	block = {command = "block @1", morphs = {"elephant"}},
	elusiveness = {command = "elusiveness @1", morphs ={"jaguar", "basilisk"}},
	fitness = {command = "fitness", morphs ={"wyvern", "jaguar", "elephant"}},
	flame = {command = "flame @target", morphs = { "basilisk"}},
	flame_icewall = {command = "flame @1", morphs ={ "basilisk"}},
	fly = {command = "fly", morphs ={"eagle", "nightingale"}},
	gaze = {command = "gaze @target @1", morphs ={"basilisk"}},
	glare = {command = "glare @target @1", morphs ={"basilisk"}},
	hoist = {command = "hoist @target", morphs ={"eagle"}},
    hoist_other = {command = "hoist @1", morphs ={"eagle"}},
	icebreath = {command = "freeze @target", morphs = {"icewyrm"}},
	icebreath_ground = {command = "freeze ground", morphs = {"icewyrm"}},
	land = {command = "land", morphs = {"eagle", "nightingale"}},
	leap = {command = "open door @1|leap @1", morphs = {"gorilla", "jaguar"}},
	melody = {command = "sing melody", morphs = {"nightingale"}},
	maul = {command = "maul @target", morphs = {"icewyrm", "jaguar"}},
	maul_targetted = {command = "maul @target @1", morphs = {"icewyrm", "jaguar"}},
	might = {command = "might", morphs = {"icewyrm", "jaguar"}},
	negate = {command = "negate @target", morphs = {"basilisk"}},
	nightsight = {command = "nightsight", morphs = {"icewyrm", "eagle", "jaguar"}},
	pound = {command = "pound @target", morphs = {"gorilla"}},
	resistance = {command = "resistance", morphs = {"jaguar", "basilisk"}},
	scanArea = {command = "scan area", morphs = {"eagle"}},
	scent = {command = "scent @target", morphs = {"icewyrm", "jaguar"}},
	sniff = {command = "sniff", morphs = {"wolverine"}},
	sprint = {command = "sprint @1", morphs = {"icewyrm", "jaguar"}},
	spring = {command = "spring @target", morphs = {"wolverine"}},
	stampede = {command = "stampede @target", morphs = {"elephant"}},
	stealth = {command = "stealth @1", morphs = {"jaguar"}},
	spy = {command = "spy @target", morphs = { "eagle"}},
	swinging = {command = "swing @1", morphs = {"gorilla"}},
	swoop = {command = "swoop @target", morphs = {"eagle"}},
	temperance = {command = "temperance", morphs = { "icewyrm"}},
	track = {command = "track @target", morphs = { "eagle"}},
	track_other = {command = "track @1", morphs = { "eagle"}},
	traverse = {command = "traverse @target", morphs = { "eagle"}},
	trumpet = {command = "trumpet @target", morphs = {"elephant"}},
	view = {command = "view", morphs = { "eagle"}},
	vitality = {command = "vitality", morphs = { "icewyrm", "jaguar"}},
	yank = {command = "yank @target", morphs = {"elephant"}},
}

local function getCurrentMorph()
	return currentMorph;
end

local function setCurrentMorph(morph)
	currentMorph = morph
end

local function usePower(ability, _1, _2, _3)
	local power = powers[ability]

	local txt = "stand|"

	if(not power) then
		cecho("<red>Attempting to use power that doesn't exist")
		return
	end

	--Make sure we are in a morph that supports this ability
	if(not table.contains(power.morphs, currentMorph)) then
		for _, v in pairs(power.morphs) do
			txt = txt .. "relax flame|morph " .. v .. "|"
			if v == "basilisk" then
                txt = txt .. "summon flame|"
            end
            break
		end
	end

	--replace target and other variables
	txt = txt .. string.gsub(power.command,"@target",tostring(target))
	if _1 then txt = string.gsub(txt,"@1",tostring(_1)) end
	if _2 then txt = string.gsub(txt,"@2",tostring(_2)) end
	if _3 then txt = string.gsub(txt,"@3",tostring(_3)) end
	Marvin.Comms.eqsend(txt)
end

local function getPetrifyScore()
	local petrifyAffs = 0;
	local eAffs = Marvin.Tracking.EnemyTracking.GetEnemyAffs();
	if (eAffs.impatience >= 0.5) then petrifyAffs = petrifyAffs + 1 end
	if (eAffs.dizziness >= 0.5) then petrifyAffs = petrifyAffs + 1 end
	if (eAffs.epilepsy >= 0.5) then petrifyAffs = petrifyAffs + 1 end
	if (eAffs.confusion >= 0.5) then petrifyAffs = petrifyAffs + 1 end
	if (eAffs.paranoia >= 0.5) then petrifyAffs = petrifyAffs + 1 end
	if (eAffs.recklessness >= 0.5) then petrifyAffs = petrifyAffs + 1 end
	if (eAffs.hallucinations >= 0.5) then petrifyAffs = petrifyAffs + 1 end

	return petrifyAffs;
end

local function checkForPetrify()
	local petrifyAffs = getPetrifyScore();
	
	local haveButterfly = Marvin.Skills.Woodlore.IsAnimalSummoned("butterfly")
	local eAffs = Marvin.Tracking.EnemyTracking.GetEnemyAffs();
	local noDizzy = eAffs.dizziness < 0.3;
	local isBlind = eAffs.blind > 0.5

	if petrifyAffs >= 4 and isBlind then return true end
	if petrifyAffs == 3 and haveButterfly and noDizzy and isBlind then return true end; 

	--display(petrifyAffs .. " " .. tostring((petrifyAffs >= 4) and affstrack.score.blind > 50))
	return false
end

--Exposed Variables and Functions
local metamorphosis = { Version = '0.2' }

metamorphosis.UsePower = usePower;
metamorphosis.SetCurrentMorph = setCurrentMorph;
metamorphosis.GetCurrentMorph = getCurrentMorph;
metamorphosis.GetPetrifyScore = getPetrifyScore;
metamorphosis.CheckForPetrify = checkForPetrify;

return metamorphosis
