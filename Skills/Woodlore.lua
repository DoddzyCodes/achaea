local traps = {
    dart = { comms = {"1 wood"} },
    snare = { comms = {"1 rope"} },
    noose = { comms = {"3 rope"} },
    catapult = { comms = {"1 rope", "1 wood"} },
    dustbomb = { comms = {"1 dust", "1 rope"} },
    horseshoe = { comms = {"1 iron", "1 rope"} },
    darts = { comms = {"3 wood", "3 rope"} }
}

local animalLongToShort = {
    ["an ebony raven"] = "raven",
    ["a grey wolf"] = "wolf",
    ["a grumpy badger"] = "badger",
    ["a gossamer butterfly"] = "butterfly",
    ["a cunning red fox"] = "fox",
    ["a small brown lemming"] = "lemming",
    
}

local affToAnimal = {
    nausea = "lemming",
    sensitivity = "raven",
    butterfly = "stupidity",
    damage = "wolf",
    badger = "bleed",
}

local animalToAff = {}
for k, v in pairs(affToAnimal) do animalToAff[v] = k end

local summonedAnimals = {}
local animalToEnrage = ""
local forceEnrage = nil

local function getAnimalShortName(txt)
    return animalLongToShort[txt:lower()]
end

local function addSummonedAnimal(animal)
    summonedAnimals[animal] = true; 
    Marvin.Comms.echo("Woodlore", "Summoned Animal: <green>" .. animal)
end

local function removeSummonedAnimal(animal, announce)
    summonedAnimals[animal] = nil;
    
    announce = announce or true
    if announce then Marvin.Comms.echo("Woodlore", "Released Animal: <red>" .. animal) end
end

local function setAnimalToBeEnraged(animal)
    animalToEnrage = animal
end

local function getRandomSummonedAnimal()
    local animals = {}; for a, _ in pairs(summonedAnimals) do animals[#animals + 1] = a end
    if #animals == 0 then return "none" else return animals[math.random(#animals)] end;
end

local function getAnimalToSummon()
    --dismissUnwantedAnimals(wantedAnimals);
    
    local ks = Marvin.Classes.Sentinel.GetKillStrategy();
    local eAffs = Marvin.Tracking.EnemyTracking.GetEnemyAffs()
    if ks == "petrify" then
        if (animalToEnrage == "butterfly" or eAffs.blind > 0.5 ) and not summonedAnimals["wolf"] then return "wolf"
        elseif animalToEnrage == "butterfly" or not summonedAnimals["butterfly"] then return "butterfly"
        else return "lemming" end; 
    else
        return "raven"
    end
    
end

local function getAnimalToEnrage()
    local ks = Marvin.Classes.Sentinel.GetKillStrategy();
    local eAffs = Marvin.Tracking.EnemyTracking.GetEnemyAffs()
    
    if forceEnrage then return forceEnrage end;
    
    if ks == "petrify" then
       if eAffs.blind < 0.5 and summonedAnimals["butterfly"] then return "butterfly"
       elseif eAffs.hallucinations < 0.5 and summonedAnimals["wolf"] then return "wolf"
       elseif eAffs.dizziness < 0.5 and summonedAnimals["butterfly"] then return "butterfly"
       else return getRandomSummonedAnimal() end   
    else
        return getRandomSummonedAnimal()
    end

    return getRandomSummonedAnimal()
end


local function removeAllSummonedAnimals()
    summonedAnimals = {}
end

local function onAnimalEnraged(animal)
    summonedAnimals[animal] = nil;
    forceEnrage = nil
end

local function setForceEnrage(animal)
    forceEnrage = animal
    Marvin.Comms.echo("Woodlore", "Will definitely enrage " .. forceEnrage)
end


local function isAnimalSummoned(animal)
    return summonedAnimals[animal] ~= nil
end

local function assembleTrap(trap)
    if not traps[trap] then
        Marvin.Comms.echo("Woodlore", "Trying to assemble invalid trap!")
    end
   
    local t = traps[trap]
   
    local commCmd = ""
    for _, comm in ipairs(t.comms) do
        commCmd = commCmd .. "outr " .. comm .. "|"
    end
   
    local cmd = "stand|" .. commCmd .. "assemble " .. trap .. " trap|"
   
    cmd = cmd .. "inrift all"
    
    Marvin.Comms.eqsend(cmd)
end


local function layTrap(trap, direction, _1)
    if not traps[trap] then
        Marvin.Comms.echo("Woodlore", "Trying to lay invalid trap!")
    end
   
    local t = traps[trap]
    local cmd = "stand|lay " .. trap .. " " .. direction
    if _1 then cmd = cmd .. " " .. _1 end
    
   
    Marvin.Comms.eqsend(cmd)

end

--Exposed Variables and Functions
local woodlore = { Version = '0.2' }

woodlore.GetAnimalShortName = getAnimalShortName;
woodlore.IsAnimalSummoned = isAnimalSummoned;

woodlore.AddSummonedAnimal = addSummonedAnimal;
woodlore.RemoveSummonedAnimal = removeSummonedAnimal;

woodlore.RemoveAllSummonedAnimals = removeAllSummonedAnimals
woodlore.DisplaySummonedAnimals = displaySummonedAnimals;

woodlore.OnAnimalEnraged = onAnimalEnraged;
woodlore.SetAnimalToBeEnraged = setAnimalToBeEnraged;
woodlore.GetAnimalToSummon = getAnimalToSummon;
woodlore.GetAnimalToEnrage = getAnimalToEnrage;
woodlore.SetForceEnrage = setForceEnrage


woodlore.AssembleTrap = assembleTrap;
woodlore.LayTrap = layTrap;

return woodlore