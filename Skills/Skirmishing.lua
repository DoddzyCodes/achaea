local limbTarget = "nothing"
local axe = "handaxe176102"
local spear = "spear190624"

local abilityToUse = nil;
local abilityMods = nil;

local currentStrike = nil;

--local spinMode = false;

local abilities = {
    returning = {weapon = axe, entSummon = true, actualCommand = "throw handaxe at &tar @venom @limb"},
    returning_outside = {weapon = axe, entSummon = true, actualCommand = "throw handaxe @1 at &tar @limb @venom"},
	trip = {weapon = spear, entSummon = true, actualCommand = "trip &tar @1"},
	grab = {weapon = axe, entSummon = false, actualCommand = "bthrow handaxe at &tar @1" },
	thrust = {weapon = spear, entSummon = true, actualCommand = "thrust &tar @limb @venom" },
	rivestrike = {weapon = spear, entSummon = true, actualCommand = "rivestrike &tar @limb @venom"},	
	rive = {weapon = spear, entSummon = false},
	ensnare = {weapon = spear, entSummon = false},
	lacerate = {weapon = spear, entSummon = true, actualCommand = "lacerate &tar @limb @venom"},
	scythe = {weapon = spear, entSummon = true},
	gouge = {weapon = spear, entSummon = true, actualCommand = "gouge &tar @limb @venom"},
	rattle = {weapon = spear, entSummon = false},
	truss = {weapon = spear, entSummon = false, preCommand = "outr 1 rope"},
	skullbash = {weapon = spear, entSummon = false},
	impale = {weapon = spear, entSummon = false},
	drag = {weapon = spear, entSummon = false},
	wrench = {weapon = spear, entSummon = false},
	doublestrike = {weapon = spear, entSummon = true, actualCommand = "doublestrike &tar @limb @venom"},
	extirpate = {weapon = spear, entSummon = false},
}

local function setNextAbilityToUse(ability, ...)
    if not abilities[ability] then
        Marvin.Comms.echo("Skirmishing", "Trying to use invalid Ability!")
        return
    end
    
    abilityToUse = ability;
	abilityMods = ...
end

local function getSkirmishingAttackCommand()
    local abilityData = abilities[abilityToUse];
    local cmd = "wield " .. abilityData.weapon .. " shield/"
    if abilityData.preCommand then cmd = cmd .. abilityData.preCommand .. "/" end
    
    if abilityData.actualCommand then cmd = cmd .. abilityData.actualCommand 
    else cmd = cmd .. abilityToUse .. " &tar" end

	if(abilityMods) then
		if abilityMods[1] then cmd = string.gsub(cmd,"@1",abilityMods[1]) end
	end

	limb = limb or Marvin.Tracking.LimbTracking.GetCurrentLimbTarget();
	if limb == "nothing" then limb = nil end

    
    return cmd
end

local function selectBestVenom()
	local ks = Marvin.Classes.Sentinel.GetKillStrategy();
	local eAffs = Marvin.Tracking.EnemyTracking.GetEnemyAffs();
	if ks == "petrify" then
		local v = ""
		local pScore = Marvin.Skills.Metamorphosis.GetPetrifyScore()
		
		if pScore < 3 and eAffs.paralysis < 0.5 then
			v = "curare"
		elseif eAffs.recklessness < 0.5 then
			v = "eurypteria"
		elseif eAffs.weariness < 0.5 then
			v = "vernalius"
		elseif eAffs.stupidity < 0.5 then
			v = "aconite"
		elseif pScore < 3 then
			v = "kalmia"
		else
			v = "kalmia"
		end
		
    	Marvin.Offensive.Venoms.SetNextVenomToApply(v)
	else
		local tempVenoms = {"curare", "aconite", "prefarar", "xentio"}
    	Marvin.Offensive.Venoms.SetNextVenomToApply(tempVenoms[math.random(#tempVenoms)])
	end
end


local function getRequiredWeapon()
    return abilities[abilityToUse].weapon;
end


function onSkirmishingAttack(strike)
	currentStrike = strike;
	enableTrigger("EnemyTrackMiss")
end

function onSkimishingAttackMissed()
	currentStrike = nil;
	disableTrigger("EnemyTrackMiss")
end

local function onPrompt()
	if currentStrike == nil then return end

	local strike = currentStrike;
	local eTrack = Marvin.Tracking.EnemyTracking;
	local eAffs = Marvin.Tracking.EnemyTracking.GetEnemyAffs();

	local affFunctions = {
		doublestrike = function() 
			if eAffs.impatience > 0.5 then eTrack.AddAff("epilepsy") else eTrack.AddAff("impatience") end
		end,
	}
	if affFunctions[strike] then affFunctions[strike]() end
	Marvin.Offensive.Venoms.OnVenomApplied();

	currentStrike = nil
end

--Exposed Variables and Functions
local skirmishing = { Version = '0.2' }

skirmishing.UseAbility = useAbility;
skirmishing.SetNextAbilityToUse = setNextAbilityToUse;
skirmishing.SelectBestVenom = selectBestVenom;

skirmishing.GetSkirmishingAttackCommand = getSkirmishingAttackCommand;
skirmishing.GetRequiredWeapon = getRequiredWeapon;


skirmishing.OnSkirmishingAttack = onSkirmishingAttack;
skirmishing.OnSkimishingAttackMissed = onSkimishingAttackMissed;
skirmishing.OnPrompt = OnPrompt;


Marvin.Events.RegisterEvent(onPrompt, "prompt", onPrompt);
return skirmishing
