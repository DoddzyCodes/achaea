local timerList = {}


function doAfter(id, time, action, reset)
  if timerList[id] and reset then
     killTimer(timerList[id])
     timerList[id] = nil
  end

  if not timerList[id] then
    timerList[id] = tempTimer(time, action);
  end
end

local Timers = { Version = '0.1a' }

Timers.DoAfter = doAfter

return Timers;
