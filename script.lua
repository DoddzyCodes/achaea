Marvin = Marvin or {}

local function LoadComponent(componentType, componentName)
	local _sep
	if string.char(getMudletHomeDir():byte()) == "/" then
		_sep = "/"
	else
		_sep = "\\"
	end -- if

	local componentFile = getMudletHomeDir() .. _sep .. "Marvin" ..
						  _sep .. componentType .. _sep .. componentName .. ".lua";


  	local properComponentType = string.title(componentType);
	local properComponentName = string.title(componentName);

	Marvin[properComponentType] = Marvin[componentType] or {}
	Marvin[properComponentType][properComponentName] = dofile(componentFile);

	if componentType == "Base" then
		Marvin[properComponentName] = Marvin[properComponentType][properComponentName];
	end

	echo("Loaded Component " .. componentType .. "/" .. properComponentName ..
				", Version: " .. Marvin[properComponentType][properComponentName].Version .. "\n")
end

LoadComponent("Base", "comms");
LoadComponent("Base", "events");
LoadComponent("Base", "partyAnnounce");
LoadComponent("Base", "FileHandling")
LoadComponent("Base", "ServerQueue")

LoadComponent("GMCP", "CharVitals");
LoadComponent("GMCP", "CharStatus");
LoadComponent("GMCP", "CharItems");
LoadComponent("GMCP", "RoomPlayers");
LoadComponent("GMCP", "IRERift");
LoadComponent("GMCP", "IRETarget");

LoadComponent("Helper", "Timers");

LoadComponent("Offensive", "targetting");
LoadComponent("Offensive", "attacking");
LoadComponent("Offensive", "venoms");

LoadComponent("Defensive", "Parrying");
LoadComponent("Defensive", "PrioSwitching");
LoadComponent("Defensive", "PipeTracking");

--LoadComponent("Skills", "runelore");
--LoadComponent("Skills", "weaponmastery");
--LoadComponent("Skills", "Crystalism");
--LoadComponent("Skills", "Artificing");
--LoadComponent("Skills", "Elementalism");
LoadComponent("Skills", "Metamorphosis");
LoadComponent("Skills", "Woodlore");
LoadComponent("Skills", "Skirmishing");

--LoadComponent("Classes", "Runewarden");
--LoadComponent("Classes", "Dragon");
--LoadComponent("Classes", "Magi");
LoadComponent("Classes", "Sentinel")

LoadComponent("Tracking", "enemyTracking")
LoadComponent("Tracking", "LimbTracking")
LoadComponent("Tracking", "peopleTracking")


LoadComponent("Misc", "Quotes");
LoadComponent("Misc", "Hunting");
LoadComponent("Misc", "WindowLayout")
