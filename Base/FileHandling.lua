require("lfs")
-- no function checks for errors.
-- you should check for them

local function isFile(name)
    if type(name)~="string" then return false end
    if not isDir(name) then
        return os.rename(name,name) and true or false
        -- note that the short evaluation is to
        -- return false instead of a possible nil
    end
    return false
end

local function isFileOrDir(name)
    if type(name)~="string" then return false end
    return os.rename(name, name) and true or false
end

local function isDir(name)
    if type(name)~="string" then return false end
    local cd = lfs.currentdir()
    local is = lfs.chdir(name) and true or false
    lfs.chdir(cd)
    return is
end

local _sep
if string.char(getMudletHomeDir():byte()) == "/" then 
	_sep = "/" 
else
	_sep = "\\" 
end -- if

local baseFilePath = getMudletHomeDir() .. _sep .. "Marvin"
local configFilePath = baseFilePath .. _sep .. "config"
local tempFilePath = baseFilePath .. _sep .. "tmp"


local function saveTable(dir, fileName, t)
    if not isDir(dir) then return false, "No existing directory" end
    local path = dir .. _sep .. fileName
    display(path)
    table.save( path, t )
    return true;
end

local function loadTable(dir, fileName)
    local path = dir .. _sep .. fileName
    display(path)
    if not isFileOrDir(path) then return nil, "There is no file at that location" end 
    local t = {}
    table.load(path, t)
    return t
end

local FileHandling = {Version = "0.1a"}

FileHandling.GetBasePath = function() return baseFilePath end
FileHandling.GetConfigPath = function() return configFilePath end
FileHandling.GetTempPath = function() return tempFilePath end

FileHandling.SaveTable = saveTable
FileHandling.LoadTable = loadTable

return FileHandling