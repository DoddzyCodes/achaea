local channelName = "pt"
local announceEnabled = true;
local comms = Marvin.Comms;

--Things to announce
local announceTarget = true;
local announceAffsGiven = true;
local announceAlert = true;


local function send(msg)
  if not announceEnabled then return end
  cmd = msg;
  svo.cc(cmd);
end

local function setAnnounceChannel(channel)
  expandalias("vconfig ccto " .. chanel)
end

local function setAnnounceEnabled(enabled)
  announceEnabled = enabled
  if enabled then
      comms.echo("Announce", "<green>Now announcing actions to party\n");
      send("I am now announcing to this channel");
  else
      comms.echo("Announce", "<red>No longer announcing actions to channel\n");
      send("I am no longer announcing to this channel");
  end
end

local function onTargetChange(target)
  if announceTarget then
    --send("Target: " .. target)
  end
end

local function onAfflictionsGiven(affList)
  if type(affList) ~= "table" then
    affList = { affList }
  end

  local affs = table.concat(affList, ", ");
  local t = Marvin.Offensive.Targetting.GetTarget();
  send(t .. " afflicted with: " .. affs)
end

Marvin.Events.RegisterEvent(onTargetChange, "targetChanged", onTargetChange);
Marvin.Events.RegisterEvent(onAfflictionsGiven, "afflictionsGiven", onAfflictionsGiven);

--Exposed Functions and variables
local partyAnnounce = { Version = '0.1a' }
  partyAnnounce.SetAnnounceChannel = setAnnounceChannel;
  partyAnnounce.SetAnnounceEnabled = setAnnounceEnabled;

  partyAnnounce.Send = send;
return partyAnnounce
