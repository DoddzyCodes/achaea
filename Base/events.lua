local registeredCallbacks = {}


local function addRegisteredCallback(name, event, fnc)
	registeredCallbacks[event] = registeredCallbacks[event] or {}
	registeredCallbacks[event][name] = fnc
end

local function removeRegisteredCallback(name, event)
	registeredCallbacks[event] = registeredCallbacks[event] or {}
	registeredCallbacks[event][name] = nil
end

local function raiseEvent(event, ...)
	registeredCallbacks[event] = registeredCallbacks[event] or {}
	for name, fnc in pairs(registeredCallbacks[event]) do
		fnc(...)
	end
end

--Hardcoded events
function onPrompt() Marvin.Events.RaiseEvent("prompt") end
registerAnonymousEventHandler("svo done with prompt", "Marvin.Events.OnPrompt")


--Exposed Variables and Functions
local events = { Version = '0.1a' }
events.RegisterEvent = addRegisteredCallback;
events.RemoveRegisteredEvent = removeRegisteredCallback;
events.RaiseEvent = raiseEvent

events.OnPrompt = onPrompt;

return events
