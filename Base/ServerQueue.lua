local dorCommand = nil
local doFirstCommand = nil
local doQueue = {}
local waitingOnQueuedCommand = false
local lastAttemptedCommand = nil
local currendCommand = nil
local aliasName = "serverdo"
local clearingQueue = false;

function sendCommand(cmd)
    if not cmd.isAlias then cmd.cmd = string.gsub(cmd.cmd, "|", "/") end

    if not cmd.isAlias then
        send("setalias " .. aliasName .. " " .. cmd.cmd, false)
        send("clearqueue eqbal|queue add eqbal " .. aliasName, false)
    else
	    send("clearqueue eqbal", false)
        send("queue add eqbal " .. cmd.cmd, false)
    end
    waitingOnQueuedCommand = true
    clearingQueue = true;
end

function pushQueue(cmd, isAlias)
    table.insert(doQueue, {cmd = cmd, isAlias = isAlias})
end

function pushQueueTop(cmd, isAlias)
    table.insert(doQueue, 1 , {cmd = cmd, isAlias = isAlias})
end

function popQueue()
    local cmd = doQueue[1];
    table.remove(doQueue, 1);
    return cmd;
end

function runNextCommand()
	if svo.stats.currenthealth <= 10 then return end
    if doFirstCommand then
        sendCommand({cmd = doFirstCommand, isAlias = false })
        doFirstCommand = nil
    elseif dorCommand then
        lastAttemptedCommand = dorCommand;
        sendCommand({cmd = dorCommand, isAlias = false})
    else
        if #doQueue > 0 then
            lastAttemptedCommand = popQueue();
            sendCommand(lastAttemptedCommand)
        end
    end
end

function onAddedToServerQueue(cmd)
    if cmd:lower() == aliasName then
        svo.deleteLineP();
    end
end


function onServerQueueRun(cmd)
    if cmd:lower() == aliasName then
        svo.deleteLineP();
        waitingOnQueuedCommand = false;
        runNextCommand();
        Marvin.Comms.echo("ServerQueue", "Running command '" .. currendCommand .. "'");
    end
end

function onServerAliasChanged(alias, cmd)
    if alias:lower() == aliasName then
        svo.deleteLineP();
        currendCommand = cmd
    end
end

function onServerQueueCleared()
    if clearingQueue then svo.deleteLineP() end
end

function addCommand(cmd, shouldRepeat, shouldClearQueue, isAlias)
    if shouldRepeat then 
        dorCommand = cmd
        Marvin.Comms.echo("ServerQueue", "Repeating command '" .. cmd .. "'");
    else
        if shouldClearQueue then clearQueue(false) end 
        pushQueue(cmd, isAlias)
        Marvin.Comms.echo("ServerQueue", "Added command '" .. cmd .. "' to the queue (" .. #doQueue .. ")");
    end
    if not waitingOnQueuedCommand then runNextCommand() end
end

function addCommandFirst(cmd)
    Marvin.Comms.echo("ServerQueue", "Added command '" .. cmd .. "' to the top of the queue (" .. #doQueue .. ")");
    doFirstCommand = cmd;
    if not waitingOnQueuedCommand then 
        runNextCommand()
    else
        if not dorCommand then pushQueueTop(lastAttemptedCommand) end
        runNextCommand();    
    end
end

function clearQueue(shouldReport)
	send("clearqueue eqbal", false)
    doQueue = {}
    dorCommand = nil
    doFirstCommand = nil
    waitingOnQueuedCommand= false;
    lastAttemptedCommand = nil

    if(shouldReport == true or shouldReport == nil) then
        Marvin.Comms.echo("ServerQueue", "Server queue has been cleared");
    end
end

local ServerQueue = { Version = '0.1a' }


ServerQueue.AddCommand = addCommand;
ServerQueue.AddCommandFirst = addCommandFirst;
ServerQueue.ClearQueue = clearQueue;
ServerQueue.OnAddedToServerQueue = onAddedToServerQueue;
ServerQueue.OnServerQueueRun = onServerQueueRun;
ServerQueue.OnServerAliasChanged = onServerAliasChanged;
ServerQueue.OnServerQueueCleared = onServerQueueCleared;

return ServerQueue;
