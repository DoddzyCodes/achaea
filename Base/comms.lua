local msgTypeColours = {
	offense = "<snow:sea_green>",
	defense = "<snow:firebrick>",
	default = "<black:LawnGreen>",
    Marvin = "<dark_slate_grey:light_grey>",
	["Limb Tracker"] = "<dark_slate_grey:black>",
}

local partyAnnounce = false

local g_echo = echo;

local function echo(t, ...)
		local color = msgTypeColours[t] or msgTypeColours.default
		cecho("\n" .. color .. "[" .. string.upper(t) .. "]:<snow:black> " .. ...)
		--g_echo("\n")
end


local function eqsend(cmd, isAlias)
	Marvin.ServerQueue.AddCommand(cmd, false, true, isAlias)
end

local function partytell(msg)
	if partyAnnounce == true then
		--wys.osend("pt " .. msg)
	end
end

local function setPartyAnnounce(enabled)
	partyAnnounce = enabled
	if partyAnnounce then
		echo("Party Announce", "Party announce <green>Enabled")
	else
		echo("Party Announce", "Party announce <red>Disabled")
	end
end

local function getPartyAnnounce() return partyAnnounce end

--Exposed Functions and variables
local comms = { Version = '0.1a' }
comms.echo = echo;
comms.eqsend = eqsend;

comms.SetPartyAnnounce = setPartyAnnounce;
comms.GetPartyAnnounce = getPartyAnnounce;
comms.PartyTell = partytell;

return comms
