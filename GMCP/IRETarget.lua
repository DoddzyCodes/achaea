local id;
local shortDesc;
local health;

local function onGMCPTargetInfo()
    id = gmcp.IRE.Target.Info.id;
    shortDesc = gmcp.IRE.Target.Info.short_desc ;
    health = gmcp.IRE.Target.Info.hpperc;
end


local IRETarget = {Version = "0.1a"}

IRETarget.GetCount = getItemCount;

IRETarget.EnableGMCP = function() 
    --display("Enableing IRE.Target")
    gmod.enableModule("Marvin.GMCP.IRETarget", "IRE.Target")
    sendGMCP([[Core.Supports.Add ["IRE.Target 1"] ]])   -- register the GMCP module independently from gmod
end

IRETarget.UpdateFromGMCP = onGMCPTargetInfo

IRETarget.GetId = function() return id end

IRETarget.GetShortDescription = function() return shortDesc end

IRETarget.GetHealth = function() return health end

registerAnonymousEventHandler("gmcp.IRE.Target.Info", "Marvin.GMCP.IRETarget.UpdateFromGMCP")

Marvin.Events.RegisterEvent(IRETarget.EnableGMCP, "onCompletedLogin", IRETarget.EnableGMCP)
return IRETarget