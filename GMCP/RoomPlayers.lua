local currentPlayers = {
    
}


local function addPlayer(player)
    currentPlayers[player] = true
end

local function removePlayer(player)
    currentPlayers[player] = nil
end

local function updatePlayers(playerList)
    currentPlayers = {}
    for _, player in pairs(playerList) do
        addPlayer(player.name)
    end
end

local function onGMCPRoomPlayerAdd()
    local d = gmcp.Room.AddPlayer
    addPlayer(d.name)
end

local function onGMCPRoomPlayerRemove()
    local d = gmcp.Room.RemovePlayer
    removePlayer(d)
end

local function onGMCPRoomPlayerList()
    local d = gmcp.Room.Players
    updatePlayers(d)
end

local function getPlayersInRoom()
    local players = {}
    for p, _ in pairs(currentPlayers) do
        players[#players + 1] = p
    end
    
    return players
end


local RoomPlayers = {Version = "0.1a"}

RoomPlayers.GetPlayersInRoom = getPlayersInRoom;
    
RoomPlayers.UpdateFromGMCPOnAdd = onGMCPRoomPlayerAdd
RoomPlayers.UpdateFromGMCPOnRemove = onGMCPRoomPlayerRemove
RoomPlayers.UpdateFromGMCPOnList = onGMCPRoomPlayerList

registerAnonymousEventHandler("gmcp.Room.AddPlayer", "Marvin.GMCP.RoomPlayers.UpdateFromGMCPOnAdd")
registerAnonymousEventHandler("gmcp.Room.RemovePlayer", "Marvin.GMCP.RoomPlayers.UpdateFromGMCPOnRemove")
registerAnonymousEventHandler("gmcp.Room.Players", "Marvin.GMCP.RoomPlayers.UpdateFromGMCPOnList")

return RoomPlayers