local status = {
    target = "",
    class = "",
}


local function updateStatusFromGMCP()
    local s = gmcp.Char.Status
    
    status.target = s.target;
    
    local oldClass = status.class;
    status.class = s.class:find("Dragon") and "dragon" or s.class:lower()
    
    if oldClass ~= status.class then
        Marvin.Events.RaiseEvent("onClassSwitch", status.class)
    end
end

local function target() return status.target end
local function class() return status.class end

local CharStatus = {Version = "0.1a"}

CharStatus.Target = target;
CharStatus.Class = class;

CharStatus.UpdateFromGMCP = updateStatusFromGMCP

registerAnonymousEventHandler("gmcp.Char.Status", "Marvin.GMCP.CharStatus.UpdateFromGMCP")

return CharStatus;