local roomItems = {
    
}

local invItems = {
    
}

local hasForcedUpdate = false;

local function addItem(loc, info)
    local t = loc == "room" and roomItems or invItems
    if loc:find("rep") then
       local container = loc:sub(4);
       
       t[container] = t[container] or {}
       t[container].items = t[container].items or {}
       t = t[container].items
    end
    
    local existedBefore = t[info.id] ~= nil
    t[info.id] = t[info.id] or {}
    t[info.id].name = info.name
    if(info.attrib) then
        t[info.id].isWorn = info.attrib:find("w") ~= nil
        t[info.id].canBeWorn = info.attrib:find("W") ~= nil
        t[info.id].isWielded = info.attrib:find("l") ~= nil
        t[info.id].isGroupable = info.attrib:find("g") ~= nil
        t[info.id].canTake = info.attrib:find("t") ~= nil
        t[info.id].isTargetable = info.attrib:find("m") ~= nil
        t[info.id].shouldAvoidTargetting = info.attrib:find("x") ~= nil
        t[info.id].isDead = info.attrib:find("d") ~= nil
        
        local wasContainerBefore = t[info.id].isContainer
        t[info.id].isContainer = info.attrib:find("c") ~= nil
        
        if(t[info.id].isContainer) then
            t[info.id].items = t[info.id].items or {}
            if not wasContainerBefore then
                sendGMCP("Char.Items.Contents " .. info.id)
                --display("asking for container contents")
            end
        end
     end
     
     Marvin.Events.RaiseEvent("onItemAdded", loc, info);
end

local function removeItem(loc, info)
    local t = loc == "room" and roomItems or invItems
    if loc:find("rep") then
        local container = loc:sub(4);
        t = t[container].items
    end
    t[info.id] = nil
    Marvin.Events.RaiseEvent("onItemRemoved", loc, info);
end

local function updateAllItems(loc, itemList)
    local t = loc == "room" and roomItems or invItems
    if loc:find("rep") then
        local container = loc:sub(4);
        t = t[container].items
    end
    
    for k, v in pairs(t) do t[k] = nil end
    
    for _, item in pairs(itemList) do
        addItem(loc, item)
    end
    
    hasForcedUpdate = false;
end

local function onGMCPAddItem()
    local d = gmcp.Char.Items.Add;
    --display("Add - ")
   -- display(d)
    addItem(d.location, d.item);
end
local function onGMCPUpdateItem()
    local d = gmcp.Char.Items.Update;
   -- display("Update - ")
    --display(d)
    addItem(d.location, d.item, true);
end

local function onGMCPRemoveItem()
    local d = gmcp.Char.Items.Remove;
   -- display("Remove - ")
   -- display(d)
    
    addItem(d.location, d.item);
end

local function onGMCPListItem()
    local d = gmcp.Char.Items.List;
    
    updateAllItems(d.location, d.items  );
end

local function getAllRoomItems() return roomItems end
local function getAllInvItems() return invItems end

local function getMatchingItems(t, comp)
    local ret = {}
    for id, item in pairs(t) do
        if(comp(id, item) == true) then
            ret[id] = item
        end
    end

    return ret
end

local function getMatchingRoomItems(comp)
    return getMatchingItems(roomItems, comp)
end

local function getMatchingInvItems(comp)
    return getMatchingItems(invItems, comp)
end

local function forceUpdateFromGMCP()
    echo("Forcing Char.Items.Inv update!")
    sendGMCP("Char.Items.Inv")
end


local CharItems = {Version = "0.1a"}

CharItems.GetAllRoomItems = getAllRoomItems;
    CharItems.GetAllInvItems = getAllInvItems;

CharItems.GetMatchingRoomItems = getMatchingRoomItems
CharItems.GetMatchingInvItems = getMatchingInvItems

CharItems.ForceUpdate = forceUpdateFromGMCP

CharItems.UpdateFromGMCPOnAdd = onGMCPAddItem
CharItems.UpdateFromGMCPOnUpdate = onGMCPUpdateItem
CharItems.UpdateFromGMCPOnRemove = onGMCPRemoveItem
CharItems.UpdateFromGMCPOnList = onGMCPListItem

registerAnonymousEventHandler("gmcp.Char.Items.Add", "Marvin.GMCP.CharItems.UpdateFromGMCPOnAdd")
registerAnonymousEventHandler("gmcp.Char.Items.Update", "Marvin.GMCP.CharItems.UpdateFromGMCPOnUpdate")
registerAnonymousEventHandler("gmcp.Char.Items.Remove", "Marvin.GMCP.CharItems.UpdateFromGMCPOnRemove")
registerAnonymousEventHandler("gmcp.Char.Items.List", "Marvin.GMCP.CharItems.UpdateFromGMCPOnList")

Marvin.Events.RegisterEvent(CharItems.ForceUpdate, "onCompletedLogin",CharItems.ForceUpdate)
return CharItems