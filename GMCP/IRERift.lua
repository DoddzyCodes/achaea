local riftItems = {}


local function changeItem(item)
    riftItems[item.name] = item.amount
end

local function updateItems(itemList)
    riftItems = {}
    for _, item in pairs(itemList) do
        changeItem(item)
    end
end

local function onGMCPIRERiftChange()
    local d = gmcp.IRE.Rift.Change
    changeItem(d)
end

local function onGMCPIRERiftList()
    local d = gmcp.IRE.Rift.List
    updateItems(d)
end

local function getItemCount(name)
 return riftItems[name]
end

local IRERift = {Version = "0.1a"}

IRERift.GetCount = getItemCount;

IRERift.ForceUpdate = function() sendGMCP("IRE.Rift.Request") end

IRERift.UpdateFromGMCPOnChange = onGMCPIRERiftChange
IRERift.UpdateFromGMCPOnList = onGMCPIRERiftList

registerAnonymousEventHandler("gmcp.IRE.Rift.Change", "Marvin.GMCP.IRERift.UpdateFromGMCPOnChange")
registerAnonymousEventHandler("gmcp.IRE.Rift.List", "Marvin.GMCP.IRERift.UpdateFromGMCPOnList")

Marvin.Events.RegisterEvent(IRERift.ForceUpdate, "onCompletedLogin", IRERift.ForceUpdate)

return IRERift