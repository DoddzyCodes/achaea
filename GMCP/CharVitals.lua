local vitals  = {
    hp = 0,
    maxhp = 0,
    mp = 0,
    maxmp = 0,
    wp = 0,
    maxwp = 0,
    ep = 0,
    maxep = 0,
    
    bal = 0,
    eq = 0,
    
    nl = 0,
    vote = 0, 
}

local function updateVitalsFromGMCP()
    local v = gmcp.Char.Vitals
    
    vitals.hp = tonumber(v.hp)
    vitals.maxhp = tonumber(v.maxhp)
    vitals.mp = tonumber(v.mp)
    vitals.maxmp = tonumber(v.maxmp)
    vitals.wp = tonumber(v.maxwp)
    vitals.maxwp = tonumber(v.maxwp)
    vitals.ep = tonumber(v.ep)
    vitals.maxep = tonumber(v.maxep)
    
    vitals.bal = v.bal == "1"
    vitals.eq = v.eq == "1"
    
    vitals.nl = tonumber(v.nl)
    vitals.vote = v.vote == "1"
end

local function health() return vitals.hp end
local function maxHealth() return vitals.maxhp end
local function percentHealth() return vitals.hp / vitals.maxhp end

local function mana() return vitals.mp end
local function maxMana() return vitals.maxmp end
local function percentMana() return vitals.mp / vitals.maxmp end


local function endurance() return vitals.ep end
local function maxEndurance() return vitals.maxep end
local function percentEndurance() return vitals.ep / vitals.maxep end

local function willpower() return vitals.wp end
local function maxWillpower() return vitals.maxwp end
local function percentWillpower() return vitals.wp / vitals.maxwp end

local function balance() return vitals.bal end
local function equalibrium() return vitals.eq end

local function experience() return vitals.nl end

local function needToVote() return vitals.vote end

local CharVitals = {Version = "0.1a"}

CharVitals.Health = health;
CharVitals.MaxHealth = maxHealth;
CharVitals.PercentHealth = percentHealth;

CharVitals.Mana = mana;
CharVitals.MaxMana = maxMana;
CharVitals.PercentMana = percentMana;

CharVitals.Endurance = endurance;
CharVitals.MaxEndurance = maxEndurance;
CharVitals.PercentEndurance = percentEndurance;

CharVitals.Willpower = willpower;
CharVitals.MaxWillpower = maxWillpower;
CharVitals.PercentWillpower = percentWillpower;

CharVitals.Balance = balance;
CharVitals.Equilibrium = equalibrium;

CharVitals.UpdateFromGMCP = updateVitalsFromGMCP

registerAnonymousEventHandler("gmcp.Char.Vitals", "Marvin.GMCP.CharVitals.UpdateFromGMCP")

return CharVitals