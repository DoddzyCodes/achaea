local function buildAttack(limb)
  lastLimb = limb or lastLimb
  local properClass = svo.me.class
  local classScript = Marvin.Classes[properClass];
  if classScript then classScript.BuildAttack(lastLimb) else
    Marvin.Comms.echo("error", "No class script for " .. svo.me.class .. "!  Can't attack");
  end
end

local function doAttack(limb)
   buildAttack(limb);
   Marvin.Comms.eqsend("attackCommand", true)
end

local function raze()
  local properClass = svo.me.class:title();
  local classScript = Marvin.Classes[properClass];
  if classScript then classScript.Raze() else
    Marvin.Comms.echo("error", "No class script for " .. svo.me.class .. "!  Can't Raze!");
  end
end

local function specialAttack(id)
  local properClass = svo.me.class:title();
  local classScript = Marvin.Classes[properClass];
  if classScript then classScript.SpecialAttack(id) else
    Marvin.Comms.echo("error", "No class script for " .. svo.me.class .. "!  Can't Raze!");
  end
end

local function onAffGained(aff)
   local percentHealth = wsys.stats.h / wsys.stats.maxh;
   if(aff == "sleeping") then return end
   buildAttack();
end

local function onAffLost(aff)
   local percentHealth = wsys.stats.h / wsys.stats.maxh;
   buildAttack();
end

Marvin.Events.RegisterEvent("attackingHandleAffed", "got aff", onAffGained);
Marvin.Events.RegisterEvent("attackingHandleAffedLost", "lost aff", onAffLost);

local attacking = { Version = '0.1a' }

 attacking.DoAttack = doAttack;
 attacking.Raze = raze;
 attacking.SpecialAttack = specialAttack;

 attacking.BuildAttack = buildAttack;

return attacking
