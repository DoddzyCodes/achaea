local lastVenomApplied
local nextVenomToApply


local affToVenom = {
clumsiness = "xentio",
recklessness = "eurypteria",
asthma = "kalmia",
shyness = "digitalis",
darkshade = "darkshade",
paralysis = "curare",
crippledarm = "epteth",
sensitivity = "prefarar",
disloyalty = "monkshood",
illness = "euphorbia",
weariness = "vernalius",
crippledleg = "epseth",
dizziness = "larskpur",
anorexia = "slike",
voyria = "voyria",
sleep = "delphinium",
stupidity = "aconite",
slickness = "gecko",
}

local venomToAff = {}
for a, v in pairs(affToVenom) do
    venomToAff[v] = a
end

local function setNextVenomToApply(venom)
    nextVenomToApply = venom
end

local function getNextVenomToApply()
    return nextVenomToApply
end

local function setAppliedVenom(venom)
  lastVenomApplied = venom
end

local function getLastVenom()
  return lastVenomApplied
end

local function onVenomApplied()
    if not lastVenomApplied then return end
    Marvin.Tracking.EnemyTracking.AddAff(venomToAff[lastVenomApplied]);
end


local function convertVenomToAffliction(venom)
    return venomToAff[venom]
end


local function convertAfflictionToVenom(venom)
    return affToVenom[venom]
end

local venoms = { Version = '0.1a' }

venoms.SetNextVenomToApply = setNextVenomToApply;
venoms.GetNextVenomToApply = getNextVenomToApply;

venoms.SetAppliedVenom = setAppliedVenom;
venoms.GetLastVenom = getLastVenom;

venoms.ConvertVenomToAffliction = convertVenomToAffliction;
venoms.ConvertAfflictionToVenom = convertAfflictionToVenom;

venoms.OnVenomApplied = onVenomApplied;

return venoms
