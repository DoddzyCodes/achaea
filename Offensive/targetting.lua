local target = "NotSet"

local function getTarget()
	return target
end

local function setTarget(targ)
	if targ == nil then return target end
	target = targ:title()
	send("SETTARGET " ..target, false)
	svo.snd("unally " .. target .. "$enemy " .. target, false)
	_G.target = target
	Marvin.Tracking.EnemyTracking.ResetTracking()
	Marvin.Comms.echo("target", "Target set to <red>" .. target)

	Marvin.Events.RaiseEvent("targetChanged", target);
end

local function setTargetClass(c)
	--[[c = c:title()
	if target == "NotSet" then	
		Marvin.Comms.echo("target", "Set a target first!")
	else
		Marvin.Tracking.PeopleTracking.UpdateDetails({name = target, class = c});
		Marvin.Comms.echo("target", "Updated " .. target .. "'s class to " .. c .. "!")
	end ]]
end

local function onSetAdventurerTarget(who)
--[[
	local d = Marvin.Tracking.PeopleTracking.GetDetails(who)
	if not d then
		Marvin.Comms.echo("target", "Have no information on this person");
	else
		Marvin.Comms.echo("target", "Class: " .. d.class .. ", Max Health: " .. d.max_health .. ", Kills: " .. d.kills_to .. ", Deaths: " .. d.deaths_to);
	end ]]
end


--Exposed Variables and Functions
local targetting = { Version = '0.1a' }
	targetting.SetTarget = setTarget;
	targetting.GetTarget = getTarget;
	
	targetting.SetTargetClass = setTargetClass;
	targetting.OnSetAdventurerTarget = onSetAdventurerTarget;
return targetting
