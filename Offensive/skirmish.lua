axeLimbTarget = "nothing"
axeNumber = "axe365255"
spinMode = false;

abilities = {
	trip = {weapon = "spear", entSummon = true },
	grab = {weapon = "axe", entSummon = false },
	thrust = {weapon = "spear", entSummon = true },
	rivestrike = {weapon = "spear", entSummon = true},	
	rive = {weapon = "spear", entSummon = false},
	ensnare = {weapon = "spear", entSummon = false},
	lacerate = {weapon = "spear", entSummon = true},
	scythe = {weapon = "spear", entSummon = true},
	gouge = {weapon = "spear", entSummon = true},
	rattle = {weapon = "spear", entSummon = false},
	truss = {weapon = "spear", entSummon = false, preCommand = "outr 1 rope"},
	skullbash = {weapon = "spear", entSummon = false},
	impale = {weapon = "spear", entSummon = false},
	drag = {weapon = "spear", entSummon = false},
	wrench = {weapon = "spear", entSummon = false},
	doublestrike = {weapon = "spear", entSummon = true},
	extirpate = {weapon = "spear", entSummon = false},
}

function useAbility(name, limb, venom)
	local ability = abilities[name]
	if(not ability) then
		cecho("<red>Attempting to use a skirmishin ability that doesn't exist")
		return
	end

	local command = "stand|wield " .. ability.weapon .. " shield|"
	if(ability.preCommand) then
		command = command .. ability.preCommand .. "|"
	end
	command = command .. name .. " " .. Marvin.Offensive.Targetting.GetTarget()
	if limb then
		command = command .. " " .. limb
		if venom then
			command = command .. " " .. venom
		end
	end
	if(ability.entSummon) then
		local cmd = Marvin.Offensive.Woodlore.GetSummonedRequiredAnimalCommand();
		if cmd and cmd ~= "" then
			command = command .. "|" .. cmd
		end 
	end

	Marvin.comms.eqsend(command);
end


function setSpinMode(state)

end	

function getSpinMode() return spinMode end

function throwAxe(outOfRoom)
	local command = ""

	if(Marvin.Offensive.Venoms.GetLastVenom() ~= Marvin.Offensive.Venoms.GetNextVenom()) then
	command = "wipe axe|";
	
	local ven = Marvin.Offensive.Venoms.GetNextVenom();
	local target = Marvin.Offensive.Targetting.GetTarget();
	if(ven ~= "") then command = command .. "envenom " .. axeNumber .. " with " .. ven .. "|" end
	end
	command = command .. "stand|wield axe shield|target " .. axeLimbTarget .. "|"
	
	if outOfRoom == true then
		local exits = gmcp.Room.Info.exits
		for dir, _ in pairs(exits) do
			command = command .. "throw handaxe " .. dir .. " at " .. target .. "|"	
		end
	else
		command = command .. "throw handaxe at " .. target .. "|"
	end

	command = command .. "target nothing"

	Marvin.comms.eqsend(command)
end

local function setLimbTarget(limb, announce)
	axeLimbTarget = limb;
	if(announce) then
		Marvin.comms.echo("Limbs", "Will now try and hit the <snow:ForestGreen>" .. limb .. "<snow:black> of your target")
	end
end

--Exposed Variables and Functions
local skirmishing = { Version = '0.1a' }


skirmishing.SetLimbTarget = setLimbTarget;

skirmishing.ThrowAxe = throwAxe;

skirmishing.SetSpinMode = setSpinMode;
skirmishing.GetSpinMode = getSpinMode;
skirmishing.UseAbility = useAbility;

return skirmishing