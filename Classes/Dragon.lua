local breathSummoned = false;
local summoningBreath = false;

local affPrios =
{
    prep = {
        venoms = {
            "paralysis",
            "clumsiness",
            "weariness"
        },
        curse = {
            {"asthma", 1},
            {"paralysis", 2},
            {"impatience", 1},
            {"sensitivity", 2},
             
        }
    },
    
    hinder = {
        
    },
    
    kill = {
        venoms = {
            "paralysis",
            "sensitivity",
            "weariness"
        },
         curse = {
            {"sensitivity", 2},
            {"paralysis", 2},
            {"asthma", 1},
            {"impatience", 1},
             
        }
    }
}


local killStrat = "kill"

local function setBreathSummoned(isSummoned)
    breathSummoned = isSummoned;
end

local function setBreathBeingSummoned(isBeingSummoned)
    breathSummoned = isBeingSummoned;
end

local function getPrioAffToGive(prioList, affToIgnore)
    local affs = Marvin.Tracking.EnemyTracking.GetEnemyAffs()
    
    local bestAff = nil;
    for _, aff in ipairs(prioList) do
        local affName = type(aff) == "table" and aff[1] or aff
        if affs[affName] < 0.5 and affName ~= affToIgnore then
            bestAff = aff
            break;
        end
    end

    return bestAff;
end

local function buildAttack(nextLimb)
  local t  = Marvin.Offensive.Targetting.GetTarget();
  
  local command = "stand";
  local affsToGive = affPrios[killStrat];
 
  
  local venomAff = getPrioAffToGive(affsToGive.venoms);
  local curseToGive = getPrioAffToGive(affsToGive.curse);
    
  if curseToGive then
    command = command .. "/dragoncurse " .. t .. " " .. curseToGive[1] .. " " .. curseToGive[2]
  end
  
  
  command = command .."/rend " .. t .. " " .. nextLimb;
  
  if venomAff then
   command = command .. " " .. Marvin.Offensive.Venoms.ConvertAfflictionToVenom(venomAff)
  end 

  command = command .. "/breathgust " .. t

  if not breathSummoned and not summoningBreath then
    command = command .. "/summon lightning"
  end
  
   send("setalias attackcommand " .. command, false)
end


local function raze()

end

local function onBecameDragon(class)
  if class ~= "dragon" then return end
send("queue prepend eqbal dragonarmour");
expandAlias("kconfig bashing attackcommand stand/wield shield285671/dragonspark &tar/sizzle &tar/overwhelm &tar/gut &tar")
expandAlias("kconfig bashing razecommand stand/vault nyen/wield shield285671/blast &tar")
end
Marvin.Events.RegisterEvent(onBecameDragon, "onClassSwitch", onBecameDragon);

local dragon = { Version = '0.1a' }
  dragon.BuildAttack = buildAttack;
  dragon.Raze = raze;
  dragon.SetBreathSummoned = setBreathSummoned;
  dragon.SetBreathBeingSummoned = setBreathBeingSummoned;
return dragon
