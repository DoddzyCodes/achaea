local killStrat = "petrify"

local allowedStrats = { 
    ["petrify"] = true, 
    ["skullbash"] = true, 
    ["dismember"] = true,  
    ["truelock"] = true
     }

local function setKillStrategy(strat)
    if not allowedStrats[strat] then
        Marvin.Comms.echo("Offensive", "Attempted to set invalid kill strategy <red>" .. strat)
        return
    end
    killStrat = stat
    Marvin.Comms.echo("Offensive", "Set kill strat to <green>" .. strat)
end

local function getKillStrategy()
    return killStrat
end

local function buildAttack()
    local cmd = "stand/"
    


    local skirmishingCmd = Marvin.Skills.Skirmishing.GetSkirmishingAttackCommand();
    Marvin.Skills.Skirmishing.SelectBestVenom();
    local venomToApply = Marvin.Offensive.Venoms.GetNextVenomToApply();
    local limbToHit = Marvin.Tracking.LimbTracking.GetCurrentLimbTarget();

    skirmishingCmd = string.gsub(skirmishingCmd,"@venom", venomToApply or "")
    
    local limb = limbToHit == "nothing" and "" or limbToHit
    skirmishingCmd = string.gsub(skirmishingCmd,"@limb", limb or "")
    
    if venomToApply then
        local w = Marvin.Skills.Skirmishing.GetRequiredWeapon();
        cmd = cmd .. "wipe " .. w
    end
    
    local animalToEnrage = Marvin.Skills.Woodlore.GetAnimalToEnrage();
    if animalToEnrage then
        Marvin.Skills.Woodlore.SetAnimalToBeEnraged(animalToEnrage)
	    cmd = cmd .. "/enrage " .. animalToEnrage .. " &tar"
    end
    cmd = cmd .. "/order entourage kill &tar/" .. skirmishingCmd
    
    local sumAnimimal = Marvin.Skills.Woodlore.GetAnimalToSummon();
	if sumAnimimal then cmd = cmd .. "/summon " .. sumAnimimal end
    
    
    send("setalias attackcommand " .. cmd, false);
end

local function specialAttack()

end

local function onBecameSentinel(class)
  if class ~= "sentinel" then return end
  send("queue prepend eqbal morph jaguar");
 end


local function onPrompt()
    if Marvin.Skills.Metamorphosis.CheckForPetrify() then
        Marvin.Comms.echo("Offensive", "<red>Petrify!!!! <white> ------ <red>Petrify!!!!")
    end
end



Marvin.Events.RegisterEvent(onBecameSentinel, "onClassSwitch", onBecameSentinel);
Marvin.Events.RegisterEvent(onPrompt, "prompt", onPrompt);

local sentinel = { Version = '0.1a' }

  sentinel.SetKillStrategy = setKillStrategy;
  sentinel.GetKillStrategy = getKillStrategy;

  sentinel.BuildAttack = buildAttack;
  sentinel.Raze = raze;
  sentinel.SpecialAttack = specialAttack;

return sentinel