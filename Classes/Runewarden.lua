local shieldStrike = nil
local useDedication = false;


local function setShieldStrikeLow ()
   shieldStrike = "low";
   if shieldStrike then Marvin.Comms.echo("offensive", "Will shieldstrike " .. shieldStrike .. " when able!") end;
end

local function setShieldStrikeMid()
   shieldStrike = "mid";
   if shieldStrike then Marvin.Comms.echo("offensive", "Will shieldstrike " .. shieldStrike .. " when able!") end;
end

local function setShieldStrikeHigh()
   shieldStrike = "high";
   if shieldStrike then Marvin.Comms.echo("offensive", "Will shieldstrike " .. shieldStrike .. " when able!") end;
end

local function tripLeft()
end

local function tripRight()
end

local function impale()
  local shield = Marvin.Tracking.WeaponTracking.GetMainShield();
  local t  = Marvin.Offensive.Targetting.GetTarget();
  Marvin.Comms.eqsend("stand|wield longsword " .. shield .. "|shieldstrike " .. t .. " mid|combination " .. t .. " impale club")
end

local function disembowel()
  local shield = Marvin.Tracking.WeaponTracking.GetMainShield();
  local t  = Marvin.Offensive.Targetting.GetTarget();
  Marvin.Comms.eqsend("stand|wield longsword " .. shield .. "|disembowel " .. t);
end

local function retardAttack()
  local t  = Marvin.Offensive.Targetting.GetTarget();
  local command  = "stand|vault nyen|wield broadsword shield84817|combination " .. t .. " rend smash"
  Marvin.Comms.eqsend(command)
end

local function specialAttack(id)
  local attacks=
  {
    setShieldStrikeMid,
    setShieldStrikeHigh,
    setShieldStrikeLow,
    retardAttack,
    disembowel,
    impale
  }
  attacks[id]();
end

local function buildAttack(nextLimb)
   local shield = Marvin.Tracking.WeaponTracking.GetMainShield();
   local swordAttack = "slice";

   local currentStrat = Marvin.Offensive.Attacking.GetCurrentStrat();

   local venom = Marvin.Offensive.Venoms.SelectNextVenom(currentStrat);
   local shieldAttack = Marvin.Skills.Weaponmastery.SelectNextShieldAttack(currentStrat, venom)

   if(shieldStrike == "low") then shieldAttack = "concuss" end

   local t  = Marvin.Offensive.Targetting.GetTarget();

   local command  = "stand/falcon slay " .. t .. "/vault nyen/wield longsword " .. shield


   if(useDedication) then command = command .. "/dedication" end

   if (not ferocity or ferocity >= 4) and shieldStrike then
      command = command .. "/shieldstrike " .. t .. " " .. shieldStrike
   end

   command = command .. "/combination " .. t

   command = command .. " " .. swordAttack

   if nextLimb then  command = command  .. " "  .. nextLimb end
   if venom then command = command .. " "  .. venom end

   command = command  .. " " .. shieldAttack;

   local engaged = Marvin.Tracking.EnemyTracking.GetEnemyState("engaged")
   if(not engaged) then command = command .. "|engage " .. t end

   send("setalias attackcommand " .. command, false)
end


local function raze()
   local t  = Marvin.Offensive.Targetting.GetTarget();
   local engaged = Marvin.Tracking.EnemyTracking.GetEnemyState("engaged")

   local shieldAction = ""
   display(enemyState)
   if Marvin.Tracking.EnemyTracking.GetEnemyState("rebounding") then
      shieldAction = "smash mid" -- Just rebounding, so keep up herb pressure
   else
      shieldAction = "trip"
   end

   command = ""

   command = "combination " .. t .. " raze " .. shieldAction;

   if(not engaged) then command = command .. "|engage " .. t end
   Marvin.Comms.eqsend(command)
end


function onBecameRunewarden(class)
  if class ~= "runewarden" then return end
  send("queue prepend eqbal wear fullplate");
  expandAlias("kconfig bashing attackcommand stand/vault nyen/wield longsword shield285671/etch rune at &tar/bulwark/collide &tar/combination &tar slice smash")
  expandAlias("kconfig bashing razecommand stand/vault nyen/wield longsword shield285671/combination &tar raze smash")
end
Marvin.Events.RegisterEvent(onBecameRunewarden, "onClassSwitch", onBecameRunewarden);

local runewarden = { Version = '0.1a' }

  runewarden.SetShieldStrike = setShieldStrike;

  runewarden.BuildAttack = buildAttack;
  runewarden.Raze = raze;
  runewarden.SpecialAttack = specialAttack;

return runewarden
