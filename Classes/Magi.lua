
local freezeLevel = -1;
local burnLevel = 0;

local function resetFreezeLevel()
    freezeLevel = -1;
end

local function addFreezeLevel(amount)
    freezeLevel = freezeLevel + amount
end

local function removeFreezeLevel()
    freezeLevel = freezeLevel - 1
    if freezeLevel < -1 then freezeLevel = -1 end
end

local function addBurnLevel(amount)
    if not Marvin.Skills.Artificing.HasDehydrate() then return end
    burnLevel = burnLevel + amount;
    display(burnLevel)
    if burnLevel >= 4 then
        cecho("\n<red>GOLEM DESTROY SOON!!")
        cecho("\n<red>GOLEM DESTROY SOON!!")
        cecho("\n<red>GOLEM DESTROY SOON!!")
    end
end

local function removeBurnLevel()
    burnLevel = burnLevel - 1;
    if burnLevel < 0 then burnLevel = 0 end
end

local function resetBurnLevel()
    burnLevel = 0;
end


local function onLimbAttack(limb)
    local t = Marvin.Offensive.Targetting.GetTarget()

    local elem = Marvin.Skills.Elementalism;
    local art = Marvin.Skills.Artificing
    
    local command = "stand|wield staff shield|"
    
    local staffAttackLine, staffType = elem.GetNextStaffAttack(t, limb);
    display(staffType)
    command = command .. staffAttackLine
    if limb then command = command .. " " .. limb end
    command = command .. "|"
    
    if freezeLevel >= 3 or (freezeLevel == 2 and staffType == "water") then
        command = command .. "golem hypothermia"
    else    
        command = command .. art.GetNextGolemAttack(t, staffType)
    end
    
    Marvin.Comms.eqsend(command)
end



local function onBecameMagi(class)
    if class ~= "magi" then return end

    send("queue prepend eqbal simultaneity");
    
end


    
function wsys.prompttags.freezeLevel()
    if freezeLevel >= 0 then
        return "<light_blue>" .. freezeLevel
    else
        return ""
    end
end

function wsys.prompttags.burnLevel()
    if burnLevel >= 0 then
        return "<red>" .. burnLevel
    else
        return ""
    end
end
   
 function wsys.prompttags.magi()
    local toReturn = ""

    local art = Marvin.Skills.Artificing;
    --return tostring(art.HasTimeFlux())
    if art.HasTimeFlux() then toReturn = toReturn .. wsys.prompttags.timeflux() end
    if freezeLevel > -1 then toReturn = toReturn .. wsys.prompttags.freezeLevel() end
    
    if art.HasDehydrate() then toReturn = toReturn .. wsys.prompttags.burnLevel() end
    
    toReturn = toReturn .. "<grey>"
    return toReturn;
end
    
 
Marvin.Events.RegisterEvent(onBecameMagi, "onClassSwitch", onBecameMagi); 
Marvin.Events.RegisterEvent(resetFreezeLevel, "targetChanged", resetFreezeLevel);

local magi = { Version = '0.1a' }
  magi.OnLimbAttack = onLimbAttack;
  magi.Raze = raze;
  
  magi.AddToFreezeLevel = addFreezeLevel;
  magi.RemoveFreezeLevel = removeFreezeLevel; 
  
  magi.AddToBurnLevel = addBurnLevel;
  magi.RemoveBurnLevel = removeBurnLevel;
  
  
  magi.ResetFreezeLevel = resetFreezeLevel;
  magi.ResetBurnLevel = resetBurnLevel;
return magi
